/***************************************************************
|	File:		MainMenuState.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MainMenuState class handles the main menu
***************************************************************/

#ifndef MAINMENUSTATE_H
#define MAINMENUSTATE_H

#include "../SGD Wrappers/SGD_GraphicsManager.h"

#include "TransState.h"
class PolygonCollider;

class Image;

/**************************************************************/
// MainMenuState class
//	- handles the main menu
//	- SINGLETON (statically allocated, not dynamic)
class MainMenuState : public TransState
{
public:
	/**********************************************************/
	// Singleton Accessor
	static MainMenuState* GetInstance( void );

	
	/**********************************************************/
	// IGameState Interface:
	virtual void	Enter( void )				override;	// load resources
	virtual void	Exit ( void )				override;	// unload resources

	virtual bool	Input( void )				override;	// handle user input
	virtual void	Update( void )	override;	// update entites
	virtual void	Render( void )				override;	// render entities / menu


private:
	/**********************************************************/
	// SINGLETON!
	MainMenuState( void )			= default;
	virtual ~MainMenuState( void )	= default;

	MainMenuState( const MainMenuState& )				= delete;	
	MainMenuState& operator= ( const MainMenuState& )	= delete;


	/**********************************************************/
	// Cursor Index
	int		m_nCursor = 0;

	PolygonCollider* pcol1;
	PolygonCollider* pcol2;

	SGD::HTexture m_hBackground;
	Image* m_pBackground;

	float m_fJoystickTimer = 0;
	float m_fJoystickTimeout = 13;
};

#endif //MAINMENUSTATE_H
