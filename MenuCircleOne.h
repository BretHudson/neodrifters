/***************************************************************
|	File:		MenuCircleOne.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle One
***************************************************************/

#pragma once

#include "MenuCircle.h"

class Text;

class MenuCircleOne :
	public MenuCircle
{
public:
	MenuCircleOne(SGD::HTexture bright, SGD::HTexture dim, int* vehicleID, int* leftGunID, int* rightGunID);
	~MenuCircleOne();

	void Update();
	void Render();

	void RenderText();
	void RenderVehicle();
	void RenderGuns();
	void RenderArrows();

private:
	SGD::HTexture m_hLeftCircleBright;
	SGD::HTexture m_hLeftCircleDim;
	Image* m_pLeftCircleBright;
	Image* m_pLeftCircleDim;

	SGD::HTexture m_hRightCircleBright;
	SGD::HTexture m_hRightCircleDim;
	Image* m_pRightCircleBright;
	Image* m_pRightCircleDim;

	SGD::HTexture m_hArrow;
	Image* m_pArrowLeft;
	Image* m_pArrowRight;

	// Vehicle arrows
	SGD::Point m_ptVehicleArrowLeft;
	SGD::Point m_ptVehicleArrowRight;
	SGD::Rectangle m_rtVehicleArrowLeft;
	SGD::Rectangle m_rtVehicleArrowRight;

	// Gun Left arrows
	SGD::Point m_ptGunLeftArrowLeft;
	SGD::Point m_ptGunLeftArrowRight;
	SGD::Rectangle m_rtGunLeftArrowLeft;
	SGD::Rectangle m_rtGunLeftArrowRight;

	// Gun Right arrows
	SGD::Point m_ptGunRightArrowLeft;
	SGD::Point m_ptGunRightArrowRight;
	SGD::Rectangle m_rtGunRightArrowLeft;
	SGD::Rectangle m_rtGunRightArrowRight;

	std::vector<Text*> m_pTexts;

	int* m_pVehicleID;
	int* m_pLeftGunID;
	int* m_pRightGunID;

	float m_fCarRotation = 0.0f;
};

