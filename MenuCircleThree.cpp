/***************************************************************
|	File:		MenuCircleThree.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle Three
***************************************************************/

#include "MenuCircleThree.h"

MenuCircleThree::MenuCircleThree(SGD::HTexture bright, SGD::HTexture dim) : MenuCircle(bright, dim)
{
}

MenuCircleThree::~MenuCircleThree()
{
}

void MenuCircleThree::Update()
{
	MenuCircle::Update();

	//
}

void MenuCircleThree::Render()
{
	MenuCircle::Render();

	//
}