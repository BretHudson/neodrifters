/***************************************************************
|	File:		MessageID.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MessageID enum declares the message enumerators
***************************************************************/

#ifndef MESSAGEID_H
#define MESSAGEID_H


/**************************************************************/
// MessageID enum
//	- enumerated list of message identifiers
enum class MessageID 
{
	MSG_CREATE_BULLET,
	MSG_CREATE_ENEMY,
	MSG_CREATE_TANK,
	MSG_DAMAGE,
	MSG_DESTROY_ENTITY,
	MSG_SCORE,
	MSG_PORTAL_DESTROYED,
	MSG_UNKNOWN, 
};


#endif //MESSAGEID_H