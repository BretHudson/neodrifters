/***************************************************************
|	File:		InstructionsState.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Instructions State
***************************************************************/

#pragma once

#include "TransState.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"

class Image;

class InstructionsState :
	public TransState
{
public:
	static InstructionsState* GetInstance();

	void Enter();
	void Exit();

	bool Input();
	void Update();
	void Render();

private:
	InstructionsState() = default;
	~InstructionsState() = default;

	InstructionsState(const InstructionsState&) = delete;
	InstructionsState& operator= (const InstructionsState&) = delete;

	SGD::HTexture m_hBackground = SGD::INVALID_HANDLE;
	Image* m_pBackground;
};

