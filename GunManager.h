/***************************************************************
|	File:		GunManager.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Gun Manager
***************************************************************/

#pragma once

#include <vector>

#include "../SGD Wrappers/SGD_GraphicsManager.h"

class Gun;
class Bullet;
class Image;

class GunManager
{
public:
	GunManager();
	~GunManager();

	void AddGun(Gun* gun);
	Gun* GetGun(unsigned int n) const;
	int GetNumGuns();

	void AddBullet(Bullet* bullet, unsigned int gunID);
	Bullet* GetBullet(unsigned int n, unsigned int gunID);
	int GetNumBullets(unsigned int gunID);

	static Image* GetIncomingImage() { return s_pIncomingImage; }

private:
	std::vector<Gun*> m_vGuns;
	std::vector<std::vector<Bullet*>> m_vBullets;

	static SGD::HTexture s_hIncomingTexture;
	static Image* s_pIncomingImage;
};

