/***************************************************************
|	File:		Input.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Handles Input and maps keys to strings
***************************************************************/

#include "Input.h"

Input::KeyMap Input::keys;
Input::ButtonMap Input::buttons;
Input::AxisMap Input::axes;

float Input::deadzone;

void Input::Update()
{
	for (AxisMapIter iter = axes.begin(); iter != axes.end(); ++iter)
	{
		float value = Input::GetAxisValue((*iter).second.type);

		bool down = false;

		if ((*iter).second.dir > 0)
			down = (value > deadzone);
		else
			down = (value < -deadzone);

		switch ((*iter).second.state)
		{
			case AxisStates::NONE:
				if (down) (*iter).second.state = AxisStates::PRESSED;
				break;
			case AxisStates::PRESSED:
				(*iter).second.state = AxisStates::DOWN;
				break;
			case AxisStates::DOWN:
				if (!down) (*iter).second.state = AxisStates::RELEASED;
				break;
			case AxisStates::RELEASED:
				(*iter).second.state = AxisStates::NONE;
				break;
		}
	}
}

void Input::RegisterKey(std::string name, SGD::Key key)
{
	KeyVector newKeys;

	for (KeyMapIter iter = keys.begin(); iter != keys.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			newKeys = (*iter).second;
			keys.erase(iter);
			break;
		}
	}

	newKeys.push_back(key);

	keys.insert(KeyPair(name, newKeys));
}

void Input::RegisterKeys(std::string name, std::initializer_list<SGD::Key> keys)
{
	for (std::initializer_list<SGD::Key>::iterator iter = keys.begin(); iter != keys.end(); ++iter)
	{
		RegisterKey(name, *iter);
	}
}

void Input::RegisterButton(std::string name, int button)
{
	ButtonVector newButtons;

	for (ButtonMapIter iter = buttons.begin(); iter != buttons.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			newButtons = (*iter).second;
			buttons.erase(iter);
			break;
		}
	}

	newButtons.push_back(button);

	buttons.insert(ButtonPair(name, newButtons));
}

void Input::RegisterButtons(std::string name, std::initializer_list<int> buttons)
{
	for (std::initializer_list<int>::iterator iter = buttons.begin(); iter != buttons.end(); ++iter)
	{
		RegisterButton(name, *iter);
	}
}

void Input::RegisterAxis(std::string negName, std::string posName, AxisTypes axis)
{
	bool insertNeg = true, insertPos = true;

	for (AxisMapIter iter = axes.begin(); iter != axes.end(); ++iter)
	{
		if ((*iter).first == negName) insertNeg = false;
		if ((*iter).first == posName) insertPos = false;
	}
	
	InputAxis neg;
	neg.dir = -1;
	neg.type = axis;
	InputAxis pos;
	pos.dir = 1;
	pos.type = axis;

	if (insertNeg) axes.insert(AxisPair(negName, neg));
	if (insertPos) axes.insert(AxisPair(posName, pos));
}

float Input::GetAxisValue(AxisTypes type)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	switch (type)
	{
		case AxisTypes::LEFTX:
			return pInput->GetLeftJoystick(0).x;
		case AxisTypes::LEFTY:
			return pInput->GetLeftJoystick(0).y;
		case AxisTypes::RIGHTX:
			return pInput->GetRightJoystick(0).x;
		case AxisTypes::RIGHTY:
			return pInput->GetRightJoystick(0).y;
	}

	return 0.0f;
}

float Input::GetAxisValue(std::string name)
{
	for (AxisMapIter iter = axes.begin(); iter != axes.end(); ++iter)
	{
		if ((*iter).first == name)
			return Input::GetAxisValue((*iter).second.type);
	}

	return 0.0f;
}

bool Input::CheckPressed(std::string name)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	for (KeyMapIter iter = keys.begin(); iter != keys.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			for (KeyVectorIter viter = (*iter).second.begin(); viter != (*iter).second.end(); ++viter)
			{
				if (pInput->IsKeyPressed(*viter))
					return true;
			}
		}
	}

	for (ButtonMapIter iter = buttons.begin(); iter != buttons.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			for (ButtonVectorIter viter = (*iter).second.begin(); viter != (*iter).second.end(); ++viter)
			{
				if (pInput->IsButtonPressed(0, *viter))
					return true;
			}
		}
	}

	// Check axes
	for (AxisMapIter iter = axes.begin(); iter != axes.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			if ((*iter).second.state == AxisStates::PRESSED)
				return true;
		}
	}

	return false;
}

bool Input::CheckDown(std::string name)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	// Check keys
	for (KeyMapIter iter = keys.begin(); iter != keys.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			for (KeyVectorIter viter = (*iter).second.begin(); viter != (*iter).second.end(); ++viter)
			{
				if (pInput->IsKeyDown(*viter))
					return true;
			}
		}
	}

	// Check buttons
	for (ButtonMapIter iter = buttons.begin(); iter != buttons.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			for (ButtonVectorIter viter = (*iter).second.begin(); viter != (*iter).second.end(); ++viter)
			{
				if (pInput->IsButtonDown(0, *viter))
					return true;
			}
		}
	}

	// Check axes
	for (AxisMapIter iter = axes.begin(); iter != axes.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			if ((*iter).second.state == AxisStates::DOWN)
				return true;
		}
	}

	return false;
}

bool Input::CheckReleased(std::string name)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	for (KeyMapIter iter = keys.begin(); iter != keys.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			for (KeyVectorIter viter = (*iter).second.begin(); viter != (*iter).second.end(); ++viter)
			{
				if (pInput->IsKeyReleased(*viter))
					return true;
			}
		}
	}

	for (ButtonMapIter iter = buttons.begin(); iter != buttons.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			for (ButtonVectorIter viter = (*iter).second.begin(); viter != (*iter).second.end(); ++viter)
			{
				if (pInput->IsButtonReleased(0, *viter))
					return true;
			}
		}
	}

	// Check axes
	for (AxisMapIter iter = axes.begin(); iter != axes.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			if ((*iter).second.state == AxisStates::RELEASED)
				return true;
		}
	}

	return false;
}

int Input::GetMouseX()
{
	return int(SGD::InputManager::GetInstance()->GetMousePosition().x);
}

int Input::GetMouseY()
{
	return int(SGD::InputManager::GetInstance()->GetMousePosition().y);
}

SGD::Point Input::GetMouseXY()
{
	return SGD::InputManager::GetInstance()->GetMousePosition();
}

bool Input::GetMousePressed(SGD::Key key)
{
	return SGD::InputManager::GetInstance()->IsKeyReleased(key);
}

bool Input::GetMouseDown(SGD::Key key)
{
	return SGD::InputManager::GetInstance()->IsKeyDown(key);
}

bool Input::GetMouseReleased(SGD::Key key)
{
	return SGD::InputManager::GetInstance()->IsKeyReleased(key);
}