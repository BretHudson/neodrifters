/***********************************************************************\
|																		|
|	File:			Anim.cpp											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Holds the data for an individual animation			|
|																		|
\***********************************************************************/

#include "Anim.h"
#include "Util.h"

Anim::Anim()
{
}

Anim::~Anim()
{
	//
}

void Anim::Update()
{
	if (m_iTimer >= m_iFramerate)
	{
		m_iTimer = 0;
		++m_iCurFrameIndex;
		m_iCurFrameIndex = Util::CircleClamp(m_iCurFrameIndex, 0, int(m_vFrames.size()) - 1);
	}
	
	++m_iTimer;
}

void Anim::AddFrame(int frame)
{
	m_vFrames.push_back(frame);
}

int Anim::GetFrame() const
{
	return m_vFrames[m_iCurFrameIndex];
}