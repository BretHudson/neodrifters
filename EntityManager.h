/***************************************************************
|	File:		EntityManager.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	EntityManager class stores & maintains all game entities
***************************************************************/

#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H


//class IEntity;			// uses IEntity*
#include "IEntity.h"	// This header file needs the header for the EntityLayerSort struct
#include <vector>		// uses std::vector

class Collider;

/**************************************************************/
// EntityManager class
//	- stores references to game entities
//	- updates & renders all game entities
class EntityManager
{
public:
	/**********************************************************/
	// Default constructor & destructor
	EntityManager( void )	= default;
	~EntityManager( void )	= default;
	

	/**********************************************************/
	// Entity Storage:
	void	AddEntity	( IEntity* pEntity, unsigned int bucket );
	void	RemoveEntity( IEntity* pEntity, unsigned int bucket );
	void	RemoveEntity( IEntity* pEntity );
	void	RemoveAll	( unsigned int bucket );
	void	RemoveAll	( void );
	

	/**********************************************************/
	// Entity Upkeep:
	void	UpdateAll	( void );
	void	RenderAll	( void );

	void	CheckCollisions( unsigned int bucket1, unsigned int bucket2 );

	void RepositionEntities(SGD::Point point, float width, float height);


private:
	/**********************************************************/
	// Not a singleton, but still don't want the Trilogy-of-Evil
	EntityManager( const EntityManager& )				= delete;
	EntityManager& operator= ( const EntityManager& )	= delete;

	
	/**********************************************************/
	// Typedefs will simplify the templates
	typedef std::vector< IEntity* >		EntityVector;
	typedef std::vector< EntityVector >	EntityTable;

	friend Collider;
	
	/**********************************************************/
	// members:
	EntityTable	m_tEntities;			// vector-of-vector-of-IEntity* (2D table)
	EntityTable	m_tRender;				// vector-of-vector-of-IEntity* (2D table)
	bool		m_bIterating = false;	// read/write lock

	struct EntityLayerSorter
	{
		bool operator()(const IEntity* a, const IEntity* b)
		{
			return a->GetLayer() > b->GetLayer();
		};
	};
};

#endif //ENTITYMANAGER_H
