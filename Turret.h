#pragma once
#include "Gun.h"
class Turret :
	public Gun
{
public:
	Turret(Entity* owner, bool automatic, std::string side);
	~Turret();
};

