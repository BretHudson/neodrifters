/***********************************************************************\
|																		|
|	File:			Image.h												|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Updates and renders a non-animated image			|
|																		|
\***********************************************************************/

#pragma once

#include "Graphic.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"

class Image :
	public Graphic
{
public:
	Image(SGD::HTexture texture, int width, int height);
	Image(SGD::HTexture texture, SGD::Size size) : Image(texture, int(size.width), int(size.height)) { };
	virtual ~Image();

	virtual void Update();
	virtual void Render(SGD::Point ptDrawPos);
	virtual Graphic* Clone();

	SGD::HTexture GetTexture(void) { return m_hTexture; }

protected:
	SGD::HTexture m_hTexture = SGD::INVALID_HANDLE;
	SGD::Rectangle m_rtDrawRect = SGD::Rectangle(0, 0, 0, 0);
};
