/***************************************************************
|	File:		Entity.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Entity class stores the shared data members
|				for all child game entities
***************************************************************/

#ifndef ENTITY_H
#define ENTITY_H


#include "IEntity.h"						// IEntity type
#include "../SGD Wrappers/SGD_Handle.h"		// HTexture type
#include <vector>

class Graphic;
class Collider;

/**************************************************************/
// Entity class
//	- parent class of all game entities, stores the shared data members
//	- velocity-based movement
class Entity : public IEntity
{
	/**********************************************************/
	// MUST have a virtual destructor to allow upcasted pointers
	// to trigger children destructors at deallocation
public:
	Entity( void )			= default;	// default constructor
	Entity(const Entity& other);
protected:
	virtual ~Entity( void );			// VIRTUAL destructor

	
public:
	/**********************************************************/
	// Entity Types:
	enum EntityType { ENT_BASE, ENT_PLAYER, ENT_ENEMY, ENT_BULLET, ENT_MISSILE, ENT_SPAWNER };

	// Operator overloads
	// Overload the < sign for layer sorting
	//friend bool operator<(const Entity& a, const Entity& b) { return a.m_iLayer > b.m_iLayer; };
	
	/**********************************************************/
	// Interface:
	//	- virtual functions for children classes to override
	virtual void	Update			( void )		override;
	virtual void	Render			( void )					override;

	virtual SGD::Rectangle GetRect	( void )	const			override;
	virtual int		GetType			( void )	const			override	{	return ENT_BASE;	}
	virtual void	HandleCollision	( const IEntity* pOther )	override;

	void AddGraphic(Graphic* graphic);
	Graphic* RemoveGraphic(Graphic* graphic);

	void AddTag(int tag);

	void SetCollider(Collider* collider);

	virtual Collider* Collide(float x, float y, int tags[]) override;
	virtual Collider* Collide(float x, float y, Collider* c) override;
	virtual Collider* Collide(float x, float y, Entity* e) override;
	

	// Children classes CANNOT override a 'final' method.
	virtual void	AddRef		( void )				final;
	virtual void	Release		( void )				final;

	
	/**********************************************************/
	// Accessors:
	SGD::Point			GetPosition	( void ) const			{	return m_ptPosition;	}
	SGD::Vector			GetVelocity	( void ) const			{	return m_vtVelocity;	}
	SGD::Size			GetSize		( void ) const			{	return m_szSize;		}
	float				GetRotation	( void ) const			{	return m_fRotation;		}
	int					GetVolume	( void ) const			{	return m_iVolume;		}
	int					GetHealth	( void ) const			{	return m_iHealth;		}
	
	// Mutators:
	void				SetPosition(SGD::Point	pos);
	void				SetVelocity	( SGD::Vector	vel	 ) 	{	m_vtVelocity	= vel;	}
	void				SetSize		( SGD::Size		size ) 	{	m_szSize		= size;	}
	virtual void		SetRotation ( float			ang  )	{	m_fRotation		= ang;	}
	void				SetVolume	( int			volume) {	m_iVolume		= volume;}
	void				ApplyDamage(int amount) { m_iHealth -= amount; }

protected:
	/**********************************************************/
	// members:
	SGD::Point			m_ptPosition	= SGD::Point{0, 0};		// 2D position
	SGD::Vector			m_vtVelocity	= SGD::Vector{0, 0};	// 2D velocity
	SGD::Size			m_szSize		= SGD::Size{0, 0};		// 2D size
	float				m_fRotation		= 0.0f;		// angle in radians

	void SetHealth(int health) { m_iHealth = health; }

	std::vector<Graphic*> m_vGraphics;

	int m_iVolume = 100;

	int m_iHealth = 100;

private:
	/**********************************************************/
	// reference count
	unsigned int		m_unRefCount	= 1;	// calling 'new' gives the prime reference
};

#endif //ENTITY_H
