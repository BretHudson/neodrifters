/***************************************************************
|	File:		Enemy.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A Vehicle that is controlled by the computer
***************************************************************/

#include "Enemy.h"
#include "Util.h"

#include "Tags.h"
#include "Collider.h"

#include "../SGD Wrappers/SGD_MessageManager.h"

#include "DestroyEntityMessage.h"
#include "ScoreMessage.h"

#include "Guns.h"

Enemy::Enemy()
{
	//
}

Enemy::Enemy(const Enemy& other) : Vehicle(other)
{
	//
}

Enemy::Enemy(const Vehicle& other) : Vehicle(other)
{
	m_fAngleInDeg = Util::RandomRange(0.0f, 360.0f);
	SetLayer(1);
	m_bTakeoff = true;
	AddTag((int)Tags::ENEMY);
	AddTag((int)Tags::VEHICLE);
}

Enemy::~Enemy()
{
	
}

void Enemy::Update()
{
	Vehicle::Update();

	if (m_iHealth <= 0)
	{
		SGD::MessageManager::GetInstance()->QueueMessage(new ScoreMessage(10));
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(this));
		
		Entity* gunLeft = (Entity*)m_pGuns->GetLeft();
		Entity* gunRight = (Entity*)m_pGuns->GetRight();

		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(gunLeft));
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(gunRight));
	}
}

void Enemy::Shoot(bool pressed, bool down)
{
	Vehicle::Shoot(pressed, down);
}
