/***************************************************************
|	File:		CreateBulletMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Create Bullet Message
***************************************************************/

#include "CreateBulletMessage.h"
#include "MessageID.h"

#include "Bullet.h"

CreateBulletMessage::CreateBulletMessage(SGD::Point point, unsigned int gunID, Vehicle* vehicle) : Message(MessageID::MSG_CREATE_BULLET)
{
	m_ptSpawn = point;
	m_iGunID = gunID;
	m_pVehicle = vehicle;
}

CreateBulletMessage::~CreateBulletMessage()
{
}