/***************************************************************
|	File:		MainMenuState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MainMenuState class handles the main menu
***************************************************************/

#include "MainMenuState.h"

#include "../SGD Wrappers/SGD_AudioManager.h"
//#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

#include "Game.h"
#include "BitmapFont.h"
#include "SelectState.h"
#include "InstructionsState.h"
#include "OptionsState.h"
#include "CreditsState.h"

#include "Entity.h"

#include "Input.h"

#include "Image.h"

#include "Util.h"

#include "PolygonCollider.h"

/**************************************************************/
// GetInstance
//	- store the ONLY instance in global memory
//	- return the only instance by pointer
/*static*/ MainMenuState* MainMenuState::GetInstance( void )
{
	static MainMenuState s_Instance;

	return &s_Instance;
}


/**************************************************************/
// Enter
//	- reset the game & load resources
/*virtual*/ void MainMenuState::Enter(void)
{
	TransState::Enter();

	Game::GetInstance()->SetCamera(SGD::Vector(0, 0));

	// Reset the cursor to the top
	// (commented out to keep the last cursor position)
	//m_nCursor = 0;

	m_hBackground = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/mainmenu/be_title_screen.png");
	m_pBackground = new Image(m_hBackground, 960, 544);

	// These polygons and such are here for testing (you can uncomment the rendering code below to play with them)
	// Switch the poly variable to get a different polygon that follows your mouse, as well as different offsets and positions relative to each

	pcol1 = new PolygonCollider();
	pcol1->SetPos(SGD::Vector(100, 20));
	pcol1->SetOffset(SGD::Vector(200, -20));
	pcol2 = new PolygonCollider();
	pcol2->SetOffset(SGD::Vector(0, 50));

	float scale = 4.0f;

	std::vector<SGD::Vector> points1;
	/*points1.push_back(SGD::Vector(-50, 0));
	points1.push_back(SGD::Vector(137, 0));
	points1.push_back(SGD::Vector(137, 301));
	points1.push_back(SGD::Vector(62, 376));
	points1.push_back(SGD::Vector(-50, 376));*/
	points1.push_back(SGD::Vector(170, 43) * scale);
	points1.push_back(SGD::Vector(159, 22) * scale);
	points1.push_back(SGD::Vector(136, 26) * scale);
	points1.push_back(SGD::Vector(132, 49) * scale);
	points1.push_back(SGD::Vector(153, 66) * scale);

	std::vector<SGD::Vector> points2;

	int poly = 4;

	switch (poly)
	{
		case -4:
			points2.push_back(SGD::Vector(0, 0));
			points2.push_back(SGD::Vector(50, 50));
			points2.push_back(SGD::Vector(0, 100));
			points2.push_back(SGD::Vector(-50, 50));
			break;
		case -3:
			points2.push_back(SGD::Vector(-50, -50));
			points2.push_back(SGD::Vector(50, -50));
			points2.push_back(SGD::Vector(50, 50));
			points2.push_back(SGD::Vector(-50, 50));
			break;
		case -2:
			points2.push_back(SGD::Vector(-50, 100));
			points2.push_back(SGD::Vector(100, 100));
			points2.push_back(SGD::Vector(70, 50));
			points2.push_back(SGD::Vector(0, 50));
			break;
		case -1:
			points2.push_back(SGD::Vector(300, 250));
			points2.push_back(SGD::Vector(450, 250));
			points2.push_back(SGD::Vector(425, 200));
			points2.push_back(SGD::Vector(350, 200));
			break;
		case 0:
			points2.push_back(SGD::Vector(180, 150));
			points2.push_back(SGD::Vector(280, 170));
			points2.push_back(SGD::Vector(290, 125));
			points2.push_back(SGD::Vector(270, 100));
			points2.push_back(SGD::Vector(200, 100));
			pcol2->SetOffset(SGD::Vector(250, 125));
			break;
		case 1:
			points2.push_back(SGD::Vector(250, 225));
			points2.push_back(SGD::Vector(100, 225));
			points2.push_back(SGD::Vector(150, 175));
			points2.push_back(SGD::Vector(220, 175));
			break;
		case 2:
			points2.push_back(SGD::Vector(0, 200));
			points2.push_back(SGD::Vector(150, 200));
			points2.push_back(SGD::Vector(120, 150));
			points2.push_back(SGD::Vector(50, 150));
			break;
		case 3:
			points2.push_back(SGD::Vector(-20, 100));
			points2.push_back(SGD::Vector(130, 100));
			points2.push_back(SGD::Vector(100, 50));
			points2.push_back(SGD::Vector(30, 50));
			break;
		case 4:
			points2.push_back(SGD::Vector(55, 27) * scale);
			points2.push_back(SGD::Vector(40, 9) * scale);
			points2.push_back(SGD::Vector(17, 15) * scale);
			points2.push_back(SGD::Vector(10, 33) * scale);
			points2.push_back(SGD::Vector(23, 57) * scale);
			points2.push_back(SGD::Vector(43, 45) * scale);
			break;
	}

	pcol1->SetPoints(points1);
	pcol2->SetPoints(points2);

	// Set background color
	SGD::GraphicsManager::GetInstance()->SetClearColor( {0, 0, 0} );	// dark gray
}

/**************************************************************/
// Exit
//	- deallocate / unload resources
/*virtual*/ void MainMenuState::Exit( void )
{
	delete pcol1;
	delete pcol2;

	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hBackground);
	delete m_pBackground;

	TransState::Exit();
}


/**************************************************************/
// Input
//	- handle user input
/*virtual*/ bool MainMenuState::Input( void )
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	// Press Escape to quit
	if (Input::CheckPressed("menuback"))
		return false;


	// Move Cursor
	m_nCursor += int(Input::CheckPressed("menudown")) - int(Input::CheckPressed("menuup"));

	m_nCursor = Util::CircleClamp(m_nCursor, 0, 4);

	// Select an option?
	if ((Input::CheckPressed("menuselect")) && (m_pTransitionEntity->GetPosition().x < -2048))
	{
		switch (m_nCursor)
		{
			case 0: // Play
				FadeToState(SelectState::GetInstance());
				return true;
			case 1: // Instructions
				FadeToState(InstructionsState::GetInstance());
				return true;
			case 2: // Options
				FadeToState(OptionsState::GetInstance());
				return true;
			case 3: // Credits
				FadeToState(CreditsState::GetInstance());
				return true;
			case 4: // Exit
				return false;
		}
	}

	TransState::Update();

	return true;	// keep playing
}


/**************************************************************/
// Update
//	- update entities / animations
/*virtual*/ void MainMenuState::Update()
{
	GetInstance()->pcol2->SetX((float)Input::GetMouseX());
	GetInstance()->pcol2->SetY((float)Input::GetMouseY());
}


/**************************************************************/
// Render
//	- render entities / menu options
/*virtual*/ void MainMenuState::Render( void )
{
	m_pBackground->Render(SGD::Point(0, 0));

	// Use the game's font
	const BitmapFont* pFont = Game::GetInstance()->GetFont();

	// Align text based on window width
	float width = Game::GetInstance()->GetScreenWidth();

	//bool overlap = Collider::Overlap(pcol1, pcol2);
	Collider* overlap = pcol1->Collide(0, 0, pcol2);

	float xPos = 500.0f;
	float yPos = 295.0f;

	pcol1->Render(SGD::Point(0, 0));
	pcol2->Render(SGD::Point(0, 0));

	if (overlap)
		pFont->Draw("OVERLAP!", { 50, 50 }, 1.0f, SGD::Color(255, 255, 255));
	else
		pFont->Draw("No overlap", { 50, 50 }, 1.0f, SGD::Color(255, 255, 255));




	// Display the game title centered at 4x scale
	pFont->Draw( "", { (width - (4 * 32 * 4.0f))/2, 50 }, 
				 4.0f, { 255, 255, 255 } );

	// Display the menu options centered at 1x scale
	pFont->Draw( "Play", { xPos, yPos }, 
				 1.0f, {255, 0, 0} );
	pFont->Draw("Instructions", { xPos, yPos + 50.0f },
				 1.0f, {255, 0, 0} );
	pFont->Draw("Options", { xPos, yPos + 100.0f },
				 1.0f, {255, 0, 0} );
	pFont->Draw("Credits", { xPos, yPos + 150.0f },
				 1.0f, {255, 0, 0} );
	pFont->Draw("Exit", { xPos, yPos + 200.0f },
				 1.0f, {255, 0, 0} );

	// Display the cursor next to the option
	pFont->Draw("=", { xPos - 50, yPos + 50 * m_nCursor },
				 1.0f, {255, 255, 0} );

	/*GetInstance()->pcol1->Render(SGD::Point(0, 0));
	GetInstance()->pcol2->Render(SGD::Point(0, 0));*/

	TransState::Render();
}
