/***********************************************************************\
|																		|
|	File:			Text.cpp											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Updates and renders a text graphic					|
|																		|
\***********************************************************************/

#include "Text.h"

#include "Game.h"

#include "BitmapFont.h"

Text::Text(std::string text, int x, int y)
{
	m_sText = text;
	m_vtOffset = { (float)x, (float)y };
}

Text::~Text()
{
	
}

void Text::Render(SGD::Point ptDrawPos)
{
	const BitmapFont* pFont;
	pFont = ((m_pFont) ? m_pFont : Game::GetInstance()->GetFont());

	SGD::Vector offset = m_vtOffset;
	offset.x = int(offset.x) + ((GetScaleX() < 0) ? m_szSize.width : 0);
	offset.y = int(offset.y) + ((GetScaleY() < 0) ? m_szSize.height : 0);

	SGD::Vector origin = m_vtOrigin;
	origin.x = origin.x;
	origin.y = origin.y;

	pFont->Draw(m_sText.c_str(), ptDrawPos + offset - origin, GetScale(), m_cColor);
}

Graphic* Text::Clone()
{
	return new Text(*this);
}