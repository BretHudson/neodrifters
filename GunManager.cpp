/***************************************************************
|	File:		GunManager.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Gun Manager
***************************************************************/

#include "GunManager.h"
#include "Gun.h"
#include "Bullet.h"
#include "Image.h"

SGD::HTexture GunManager::s_hIncomingTexture = SGD::INVALID_HANDLE;
Image* GunManager::s_pIncomingImage;

GunManager::GunManager()
{
	if (!s_pIncomingImage)
	{
		s_hIncomingTexture = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/weapons/be_incoming.png");
		s_pIncomingImage = new Image(s_hIncomingTexture, 12, 24);
		s_pIncomingImage->CenterOrigin();
	}
}

GunManager::~GunManager()
{
	// Release Guns
	for (std::vector<Gun*>::iterator iter = m_vGuns.begin(); iter != m_vGuns.end(); ++iter)
		(*iter)->Release();

	// Release Bullets
	for (std::vector<std::vector<Bullet*>>::iterator iter = m_vBullets.begin(); iter != m_vBullets.end(); ++iter)
	{
		for (std::vector<Bullet*>::iterator bulletIter = (*iter).begin(); bulletIter != (*iter).end(); ++bulletIter)
		{
			(*bulletIter)->Release();
		}
	}

	if (s_pIncomingImage)
	{
		SGD::GraphicsManager::GetInstance()->UnloadTexture(s_hIncomingTexture);
		delete s_pIncomingImage;
	}
}

void GunManager::AddGun(Gun* gun)
{
	m_vGuns.push_back(gun);
}

Gun* GunManager::GetGun(unsigned int n) const
{
	if (n > m_vGuns.size())
		return nullptr;
	return m_vGuns[n];
}

int GunManager::GetNumGuns()
{
	return m_vGuns.size();
}

void GunManager::AddBullet(Bullet* bullet, unsigned int gunID)
{
	if (m_vBullets.size() <= gunID)
		m_vBullets.resize(gunID + 1);

	/*while (m_vBullets.size() <= gunID)
	{
		m_vBullets.resize(gunID + 1);
		std::vector<Bullet*> vector;
		m_vBullets.push_back(vector);
	}*/

	m_vBullets[gunID].push_back(bullet);
}

Bullet* GunManager::GetBullet(unsigned int n, unsigned int gunID)
{
	if (n > m_vBullets[gunID].size())
		return nullptr;
	return m_vBullets[gunID][n];
}

int GunManager::GetNumBullets(unsigned int gunID)
{
	return m_vBullets[gunID].size();
}