/***************************************************************
|	File:		CreateTankMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Create Tank Message
***************************************************************/

#include "CreateTankMessage.h"

#include "MessageID.h"

CreateTankMessage::CreateTankMessage() : Message(MessageID::MSG_CREATE_TANK)
{	
}

CreateTankMessage::~CreateTankMessage()
{
}