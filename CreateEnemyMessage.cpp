/***************************************************************
|	File:		CreateEnemyMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Create Enemy Message
***************************************************************/

#include "CreateEnemyMessage.h"
#include "MessageID.h"

CreateEnemyMessage::CreateEnemyMessage(SGD::Point point) : Message(MessageID::MSG_CREATE_ENEMY)
{
	m_ptSpawnPoint = point;
}

CreateEnemyMessage::~CreateEnemyMessage()
{
}
