/***********************************************************************\
|																		|
|	File:			Util.h												|
|	Author:			Bret Hudson										|
|	Purpose:		Holds useful math functions to speed up development |
|																		|
\***********************************************************************/

#pragma once

#include <iostream>

typedef unsigned char Byte;

class Util
{
public:
	template<class Type>
	static Type Clamp(Type var, Type min, Type max);

	template<class Type>
	static Type CircleClamp(Type var, Type min, Type max);

	template<class Type>
	static Type Max(Type var, Type max);

	template<class Type>
	static Type Random(Type n);
	
	template<class Type>
	static Type RandomRange(Type min, Type max);
};

template<class Type>
Type Util::Clamp(Type var, Type min, Type max)
{
	if (var < min) return min;
	if (var > max) return max;
	return var;
}

template<class Type>
Type Util::CircleClamp(Type var, Type min, Type max)
{
	if (var < min) return max;
	if (var > max) return min;
	return var;
}

template<class Type>
Type Util::Max(Type var, Type max)
{
	return Util::Clamp(var, Type(0), max);
}

template<class Type>
Type Util::Random(Type n)
{
	return RandomRange(Type(0), n);
}

template<class Type>
Type Util::RandomRange(Type min, Type max)
{
	return static_cast<Type>((static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * (max - min) + min);
}