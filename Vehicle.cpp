/***************************************************************
|	File:		Vehicle.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A base class for Vehicles
***************************************************************/

#include "Vehicle.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

#include "Game.h"
#include "Input.h"

#include "Guns.h"
#include "Graphic.h"
#include "Image.h"

#include "Collider.h"
#include "PolygonCollider.h"

#include "Tags.h"

Vehicle::Vehicle()
{
	SetLayer(15);
	SetRotation(0.0f);
}

Vehicle::Vehicle(const Vehicle& other) : Entity(other)
{
	m_sName = other.m_sName;
	m_iID = other.m_iID;

	m_fMaxspeed = other.m_fMaxspeed;
	m_fIdleSpeed = other.m_fIdleSpeed;
	
	m_fTurnAmount = other.m_fTurnAmount;

	PolygonCollider* poly = new PolygonCollider();
	
	std::vector<SGD::Vector> points;
	points.push_back(SGD::Vector(-20, -20));
	points.push_back(SGD::Vector(20, -20));
	points.push_back(SGD::Vector(20, 20));
	points.push_back(SGD::Vector(-20, 20));
	poly->SetPoints(points);
	
	AddTag((int)Tags::VEHICLE);
	
	SetCollider(poly);
}

Vehicle::~Vehicle()
{
	delete m_pGuns;
}

void Vehicle::Update()
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	// This was -= but after some changes to the SetRotation, it's now += ... why?
	if (m_fSpeed > 0)
		m_fAngleInDeg += m_fTurnAmount * ((m_bEbrake) ? 2 : 1) * m_iTurnDir;

	if ((m_bTakeoff) && (m_fSpeed <= m_fIdleSpeed))
	{
		m_fSpeed = m_fMaxspeed;
		Screenshake(40.0f);
	}

	if ((m_bGas) && (m_fSpeed < m_fMaxspeed))
		m_fSpeed += 0.25f;
	 else if ((m_fSpeed > m_fIdleSpeed) || (m_bBrake) || (pInput->GetLeftJoystick(0).y > 0.08) || m_fSpeed < 2.0)
		m_fSpeed -= 0.25f;
	
	if (m_bEbrake)
		m_fSpeed -= 0.45f;

	if (m_fSpeed < 0)
		m_fSpeed = 0;
	
	if (m_fSpeed > m_fIdleSpeed)
		Screenshake(0.5f);

	SGD::Vector newVelocity = SGD::Vector(1, 0) * m_fSpeed;
	SetRotation(m_fAngleInDeg * 3.14159f / 180);
	newVelocity.Rotate(GetRotation());
	SetVelocity(newVelocity);

	SGD::Point colliderOffset(0, 0);
	SGD::Vector offsetVector(20, 0);
	offsetVector.Rotate(m_fRotation);
	colliderOffset -= offsetVector;
	if (GetCollider())
		GetCollider()->SetOffset(colliderOffset);

	Entity::Update();

	int* tags = new int[2];
	tags[0] = (int)Tags::SOLID;
	tags[1] = -1;
	Collider* collider = Collide(0.0f, 0.0f, tags);
	if (collider)
	{
		SGD::Vector toSolid = collider->GetEntity()->GetPosition() - m_ptPosition;
		SGD::Vector orientation = SGD::Vector(1, 0);
		orientation.Rotate(m_fRotation);
		float steering = orientation.ComputeSteering(toSolid);

		if (steering > 0)
		{
			m_fRotation -= SGD::PI * 0.16f;
			m_fAngleInDeg -= 60;
		}
		else
		{
			m_fRotation += SGD::PI * 0.16f;
			m_fAngleInDeg += 60;
		}

		//m_fRotation += SGD::PI;
		SetRotation(m_fRotation);
		m_fSpeed *= 0.4f;
	}
	delete[] tags;

	PositionGuns();

	Shoot(false, false);
}

void Vehicle::Screenshake(float shake)
{
	//
}

void Vehicle::PositionGuns()
{
	if (m_pGuns)
	{
		SGD::Point leftPos = GetPosition();
		SGD::Vector leftAdd = SGD::Vector(30, -20);
		leftAdd.Rotate(m_fRotation);
		leftPos += leftAdd;

		SGD::Point rightPos = GetPosition();
		SGD::Vector rightAdd = SGD::Vector(30, 20);
		rightAdd.Rotate(m_fRotation);
		rightPos += rightAdd;

		m_pGuns->PositionGuns(leftPos, rightPos, m_fRotation);
	}
}

void Vehicle::Shoot(bool pressed, bool down)
{
	if (m_pGuns)
		m_pGuns->Shoot(pressed, down);
}

void Vehicle::SetRotation(float angle)
{
	Entity::SetRotation(angle);
	if (m_vGraphics.size() > 0)
		m_vGraphics[0]->SetRotation(GetRotation());
}

SGD::Point Vehicle::GetCenter() const
{
	Graphic* graphic = m_vGraphics[0];

	SGD::Vector centerOffset;
	centerOffset.x = graphic->GetWidth() / 2.0f - graphic->GetOriginX();
	centerOffset.Rotate(m_fRotation);

	return m_ptPosition + centerOffset;
}

void Vehicle::SetGuns(Guns* guns)
{
	m_pGuns = guns;
	m_pGuns->SetOwner(this);
}