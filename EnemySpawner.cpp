/***************************************************************
|	File:		EnemySpawner.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	AKA Portal, spawns Enemy objects
***************************************************************/

#include "EnemySpawner.h"

#include "Game.h"

#include "Image.h"

#include "VehicleManager.h"
#include "GunManager.h"
#include "Enemy.h"

#include "../SGD Wrappers/SGD_MessageManager.h"
#include "CreateEnemyMessage.h"
#include "DestroyEntityMessage.h"
#include "PortalDestroyedMessage.h"
#include "ScoreMessage.h"

#include "PolygonCollider.h"
#include "Tags.h"

EnemySpawner::EnemySpawner(int x, int y, int id, SGD::HTexture texture)
{
	SetPosition(SGD::Point((float)x, (float)y));
	
	m_iID = id;

	Image* image = new Image(texture, 140, 148);
	image->CenterOrigin();
	AddGraphic(image);

	SetLayer(5);

	m_iTimer = int(id * (m_iTimeout * 0.25f));

	PolygonCollider* poly = new PolygonCollider();
	poly->AddTag((int)Tags::SOLID);
	poly->AddTag((int)Tags::PORTAL);
	
	std::vector<SGD::Vector> points;
	SGD::Vector v(72, 0);
	points.push_back(v);

	for (int i = 0; i < 7; ++i)
	{
		v.Rotate(SGD::PI * 0.25f);
		points.push_back(v);
	}
	
	poly->SetPoints(points);

	SetCollider(poly);

	SetHealth(300);
}

EnemySpawner::~EnemySpawner()
{
}

void EnemySpawner::Update()
{
	++m_iTimer;

	if (m_iTimer >= m_iTimeout)
	{
		m_iTimer = 0;
		
		SGD::MessageManager::GetInstance()->QueueMessage(new CreateEnemyMessage(GetPosition()));
	}

	if (m_iHealth <= 0)
	{
		SGD::MessageManager::GetInstance()->QueueMessage(new ScoreMessage(75));
		SGD::MessageManager::GetInstance()->QueueMessage(new PortalDestroyedMessage(this));
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(this));
	}
}