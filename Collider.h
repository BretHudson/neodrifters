/***********************************************************************\
|																		|
|	File:			Collider.h											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		An abstract base class for colliders				|
|																		|
\***********************************************************************/

#pragma once

#include "../SGD Wrappers/SGD_Geometry.h"

#include <list>

class Entity;

// TODO: Implement the Clone system

class Collider
{
public:
	virtual ~Collider() = 0;

	virtual void Render(SGD::Point drawPoint) { };

	// Accessors
	SGD::Vector GetPos() const { return m_vtOrigin; }
	float GetX() const { return m_vtOrigin.x; }
	float GetY() const { return m_vtOrigin.y; }

	SGD::Vector GetOffset() const { return m_vtOffset; }
	float GetOffsetX() const { return m_vtOffset.x; }
	float GetOffsetY() const { return m_vtOffset.y; }

	// Mutators
	virtual void SetPos(SGD::Vector pos) { m_vtOrigin = pos; }
	virtual void SetPos(SGD::Point pos) { m_vtOrigin = SGD::Vector(pos.x, pos.y); }
	void SetX(float x) { m_vtOrigin.x = x; }
	void SetY(float y) { m_vtOrigin.y = y; }

	virtual void SetOffset(SGD::Vector offset) { m_vtOffset = offset; }
	virtual void SetOffset(SGD::Point offset) { m_vtOffset = SGD::Vector(offset.x, offset.y); }
	void SetOffsetX(float x) { m_vtOffset.x = x; }
	void SetOffsetY(float y) { m_vtOffset.y = y; }

	// Tag system
	void AddTag(int tag);
	bool HasTag(int tag);
	void RemoveTag(int tag);

	Entity* GetEntity(void) const { return m_pEntity; }
	void SetEntity(Entity* entity) { m_pEntity = entity; }

	Collider* Collide(float x, float y, int tags[]);
	Collider* Collide(float x, float y, Collider* c);
	Collider* Collide(float x, float y, Entity* e);

	static bool Overlap(Collider* collider1, Collider* collider2);

protected:
	Collider() = default;
	Collider(const Collider& other);
	Collider& operator=(const Collider& other);

	SGD::Vector m_vtOrigin;
	SGD::Vector m_vtOffset;

private:
	std::list<int> m_lTags;

	Entity* m_pEntity = nullptr;
};