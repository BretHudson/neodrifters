/***************************************************************
|	File:		CreditsState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A IGameState that is the credits
***************************************************************/

#include "CreditsState.h"

#include "Image.h"

#include "Input.h"

#include "Game.h"
#include "MainMenuState.h"

CreditsState* CreditsState::GetInstance()
{
	static CreditsState s_Instance;
	return &s_Instance;
}

void CreditsState::Enter()
{
	m_hBackground = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/credits/be_credits.png");
	m_pBackground = new Image(m_hBackground, 960, 544);
}

void CreditsState::Exit()
{
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hBackground);
	delete m_pBackground;
}

bool CreditsState::Input()
{
	// Press Escape to return to Main Menu
	if (Input::CheckPressed("menuback"))
	{
		Game::GetInstance()->ChangeState(MainMenuState::GetInstance());

		return true;
	}

	return true;
}

void CreditsState::Update()
{
	//
}

void CreditsState::Render()
{
	m_pBackground->Render(SGD::Point(0, 0));
}