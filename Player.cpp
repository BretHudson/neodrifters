/***************************************************************
|	File:		Player.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A Vehicle that is controlled by the player
***************************************************************/

#include "Player.h"

#include "Game.h"
#include "Input.h"

#include "Spritemap.h"

#include "Tags.h"
#include "Collider.h"

#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "DestroyEntityMessage.h"

Player::Player()
{
}

Player::Player(const Player& other) : Vehicle(other)
{
	
}

Player::Player(const Vehicle& other) : Vehicle(other)
{
	AddTag((int)Tags::PLAYER);
	AddTag((int)Tags::VEHICLE);

	m_hDriving = SGD::AudioManager::GetInstance()->LoadAudio("resource/audio/motor_idling.wav");
	SGD::AudioManager::GetInstance()->PlayAudio(m_hDriving, true);
}

Player::~Player()
{
	if (m_hDriving != SGD::INVALID_HANDLE)
		SGD::AudioManager::GetInstance()->UnloadAudio(m_hDriving);
}

void Player::Update()
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	SGD::AudioManager::GetInstance()->SetAudioVolume(m_hDriving, m_iVolume >> 1);

	m_bTakeoff = Input::CheckPressed("gas");
	m_bGas = Input::CheckDown("gas");
	m_bBrake = Input::CheckDown("brake");
	m_bEbrake = Input::CheckDown("ebrake");
	
	int left = int((Input::CheckDown("left")) || (pInput->GetLeftJoystick(0).x < -0.08));
	int right = int((Input::CheckDown("right")) || (pInput->GetLeftJoystick(0).x > 0.08));
	m_iTurnDir = right - left;

	Vehicle::Update();

	if (m_iHealth <= 0)
	{
		/*SGD::MessageManager::GetInstance()->QueueMessage(new ScoreMessage(10));
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(this));

		Entity* gunLeft = (Entity*)m_pGuns->GetLeft();
		Entity* gunRight = (Entity*)m_pGuns->GetRight();

		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(gunLeft));
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(gunRight));*/
	}
}

void Player::Screenshake(float shake)
{
	Game::GetInstance()->AddScreenshake(shake);
}

void Player::Shoot(bool pressed, bool down)
{
	Vehicle::Shoot(Input::CheckPressed("shoot"), Input::CheckDown("shoot"));
}

// HURT = Screenshake x 10
// SHOOT = Screenshake x 