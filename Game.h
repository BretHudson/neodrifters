/***************************************************************
|	File:		Game.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Game class controls the SGD wrappers
|				& runs the game state machine
***************************************************************/

#ifndef GAME_H
#define GAME_H


/**************************************************************/
// Forward class declaration
//	- tells the compiler that the type exists
//	- allows us to create pointers or references
class BitmapFont;
class IGameState;
class GunManager;
class VehicleManager;
class TiXmlElement;
class Text;

#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"

#include <vector>

/**************************************************************/
// Game class
//	- runs the game logic
//	- controls the SGD wrappers
//	- SINGLETON!
//		- there is only ONE instance
//		- global access method (GetInstance)
class Game
{
public:
	/**********************************************************/
	// Singleton Accessors:
	static Game*	GetInstance(void);
	static void		DeleteInstance(void);

	static GunManager*	GetGunManager(void);
	static VehicleManager*	GetVehicleManager(void);


	/**********************************************************/
	// Setup, Play, Cleanup:
	bool Initialize(float width, float height, float frameRate);
	void InitControls();
	
	void LoadXML();
	void CreateGun(TiXmlElement* currentGun);
	void CreateBullet(TiXmlElement* currentBullet, int numGuns);
	void CreateVehicle(TiXmlElement* currentVehicle);

	int	 Update(void);
	void Terminate(void);


	float				GetElapsedTime(void) const { return elapsedTime; }

	/**********************************************************/
	// Screen Size Accessors:
	float				GetScreenWidth(void) const	{ return m_fScreenWidth; }
	float				GetScreenHeight(void) const	{ return m_fScreenHeight; }

	/**********************************************************/
	// Camera Accessors:
	SGD::Vector			GetCamera(void) const	{ return m_vtCameraOffset; }
	// Camera Modifiers:
	void				SetCamera(SGD::Vector offset)	{ m_vtCameraOffset = offset; }
	void				SetCamera(SGD::Point point);
	void				AddScreenshake(float shake) { shakeAmount += shake; }

	// Font Accessor:
	const BitmapFont*	GetFont			( void ) const	{	return m_pFont;			}

	void Won();
	void Lose();

	
	/**********************************************************/
	// Game State Machine:
	//	- can ONLY be called by the state's Input, Update, or Render methods!!!
	void ChangeState( IGameState* pNewState );

	IGameState* GetState(void) const { return m_pCurrState; }


private:
	/**********************************************************/
	// Singleton Object:
	static Game*	s_pInstance;
	static GunManager* s_pGunManager;
	static VehicleManager* s_pVehicleManager;

	Game( void )	= default;		// default constructor
	~Game( void )	= default;		// destructor

	Game( const Game& )				= delete;	// copy constructor
	Game& operator= ( const Game& )	= delete;	// assignment operator

	

	/**********************************************************/
	// Screen Size
	float					m_fScreenWidth = 1;
	float					m_fScreenHeight = 1;
	bool m_bFullscreen = false;

	/**********************************************************/
	// Camera
	SGD::Vector				m_vtCameraOffset = SGD::Vector(0, 0);
	float					shakeAmount = 0;
	

	/**********************************************************/
	// Game Font
	BitmapFont*				m_pFont			= nullptr;


	/**********************************************************/
	// Current Game State
	IGameState*				m_pCurrState	= nullptr;


	/**********************************************************/
	// Game Time
	unsigned long			m_ulGameTime	= 0;
	float					elapsedTime = 0;
	float					frameRate;
	
	std::vector<SGD::HTexture> m_vGunTextures;
	std::vector<SGD::HTexture> m_vBulletTextures;
	std::vector<SGD::HTexture> m_vVehicleTextures;

	SGD::HAudio m_hWon = SGD::INVALID_HANDLE;
	SGD::HAudio m_hLose = SGD::INVALID_HANDLE;

	bool m_bWon = false;
	bool m_bLose = false;

	Text* m_pWonText;
	Text* m_pLoseText;
};

#endif //GAME_H
