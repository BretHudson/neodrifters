/***********************************************************************\
|																		|
|	File:			Text.h												|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Updates and renders a text graphic					|
|																		|
\***********************************************************************/

#pragma once

#include "Graphic.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"

#include <string>

class BitmapFont;

class Text :
	public Graphic
{
public:
	Text(std::string text) : Text(text, 0, 0) { };
	Text(std::string text, int x, int y);
	virtual ~Text();
	
	virtual void Update() { };
	virtual void Render(SGD::Point ptDrawPos);
	virtual Graphic* Clone();

	// Accessors
	std::string GetText(void) const { return m_sText; }
	BitmapFont* GetFont(void) const { return m_pFont; }
	SGD::Color GetColor(void) const { return m_cColor; }
	float GetScale(void) const { return m_ptScale.x; }

	// Mutators
	void SetText(std::string text) { m_sText = text; }
	void SetFont(BitmapFont* font) { m_pFont = font; }
	void SetColor(SGD::Color color) { m_cColor = color; }
	void SetScale(float scale) { m_ptScale.x = scale; }

protected:
	std::string m_sText;
	BitmapFont* m_pFont = nullptr;
	SGD::Color m_cColor = SGD::Color(255, 255, 255);
};