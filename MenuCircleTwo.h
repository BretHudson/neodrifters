/***************************************************************
|	File:		MenuCircle.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle Two
***************************************************************/

#pragma once
#include "MenuCircle.h"
class MenuCircleTwo :
	public MenuCircle
{
public:
	MenuCircleTwo(SGD::HTexture bright, SGD::HTexture dim);
	~MenuCircleTwo();

	void Update();
	void Render();
};
