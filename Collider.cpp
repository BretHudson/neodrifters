/***********************************************************************\
|																		|
|	File:			Collider.cpp										|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		An abstract base class for colliders				|
|																		|
\***********************************************************************/

#include "Collider.h"
#include "PolygonCollider.h"

#include "Entity.h"

#include "Game.h"

#include "IGameState.h"
#include "GameplayState.h"
#include "EntityManager.h"

Collider::~Collider()
{
	//
}

void Collider::AddTag(int tag)
{
	if (!HasTag(tag))
		m_lTags.push_back(tag);
}

bool Collider::HasTag(int tag)
{
	for (std::list<int>::iterator iter = m_lTags.begin(); iter != m_lTags.end(); ++iter)
	{
		if (*iter == tag)
			return true;
	}

	return false;
}

void Collider::RemoveTag(int tag)
{
	m_lTags.push_back(tag);
}

Collider* Collider::Collide(float x, float y, int tags[])
{
	GameplayState* state = dynamic_cast<GameplayState*>(Game::GetInstance()->GetState());

	SGD::Vector originalOffset = m_vtOffset;
	SGD::Vector tempOffset = m_vtOffset;
	tempOffset += SGD::Vector(x, y);
	m_vtOffset = tempOffset;

	Collider* collider = nullptr;

	if (state)
	{
		EntityManager* manager = state->GetEntityManager();

		/*for (std::vector<std::vector<IEntity*>>::iterator iter = manager->m_tEntities.begin(); iter != manager->m_tEntities.end(); ++iter)
		{
		if (collider) break;
		*/
		for (std::vector<IEntity*>::iterator entityIter = manager->m_tEntities[0].begin(); entityIter != manager->m_tEntities[0].end(); ++entityIter)
		{
			if (collider) break;

			Collider* otherCollider = (*entityIter)->GetCollider();
			if (!otherCollider) continue;

			int i = 0;
			while (true)
			{
				int tag = tags[i];
				if (tag == -1) break;
				if ((otherCollider->HasTag(tag)) && (Overlap(this, otherCollider)))
				{
					collider = otherCollider;
					break;
				}
				++i;
			}
		}
		//}
	}

	m_vtOffset = originalOffset;

	return collider;
}

Collider* Collider::Collide(float x, float y, Collider* c)
{
	SGD::Vector originalOffset = GetOffset();
	SGD::Vector tempOffset = originalOffset;
	tempOffset += SGD::Vector(x, y);
	SetOffset(tempOffset);

	Collider* collider = nullptr;

	if (Overlap(this, c))
		collider = c;

	SetOffset(originalOffset);

	return collider;
}

Collider* Collider::Collide(float x, float y, Entity* e)
{
	SGD::Vector originalOffset = GetOffset();
	SGD::Vector tempOffset = originalOffset;
	tempOffset += SGD::Vector(x, y);
	SetOffset(tempOffset);

	Collider* collider = nullptr;

	if (Overlap(this, e->GetCollider()))
		collider = e->GetCollider();


	SetOffset(originalOffset);

	return collider;
}

bool Collider::Overlap(Collider* collider1, Collider* collider2)
{
	if ((collider1 == nullptr) || (collider2 == nullptr) || (collider1 == collider2))
		return nullptr;

	PolygonCollider* polyCollider1 = dynamic_cast<PolygonCollider*>(collider1);
	PolygonCollider* polyCollider2 = dynamic_cast<PolygonCollider*>(collider2);

	if ((polyCollider1) && (polyCollider2))
	{
		std::vector<Axis> axes1 = polyCollider1->GetAxes();
		std::vector<Axis> axes2 = polyCollider2->GetAxes();

		for (unsigned int i = 0; i < axes1.size(); ++i)
		{
			Axis axis = axes1[i];

			Projection p1 = polyCollider1->Project(axis);
			Projection p2 = polyCollider2->Project(axis);

			if (!p1.Overlap(p2))
				return false;
		}

		for (unsigned int i = 0; i < axes2.size(); ++i)
		{
			Axis axis = axes2[i];

			Projection p1 = polyCollider1->Project(axis);
			Projection p2 = polyCollider2->Project(axis);

			if (!p1.Overlap(p2))
				return false;
		}
	}

	return true;
}