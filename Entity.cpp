/***************************************************************
|	File:		Entity.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Entity class stores the shared data members
|				for all child game entities
***************************************************************/

#include "Entity.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include <cassert>

#include "Game.h"
#include "Graphic.h"
#include <cmath>
#include "Text.h"

#include "Collider.h"


Entity::Entity(const Entity& other)
{
	for (std::vector<Graphic*>::const_iterator iter = other.m_vGraphics.begin(); iter != other.m_vGraphics.end(); ++iter)
	{
		m_vGraphics.push_back((*iter)->Clone());
	}
}

/**************************************************************/
// DESTRUCTOR
/*virtual*/ Entity::~Entity( void )
{
	assert( m_unRefCount == 0 && "~Entity - entity was destroyed while references remain!" );

	for (std::vector<Graphic*>::iterator iter = m_vGraphics.begin(); iter != m_vGraphics.end(); ++iter)
	{
		delete *iter;
	}

	m_vGraphics.clear();

	if (m_pCollider)
		delete m_pCollider;
	m_pCollider = nullptr;
}


/**************************************************************/
// Update
//	- move the entity's position by its velocity
/*virtual*/ void Entity::Update()
{
	m_ptPosition += m_vtVelocity;

	for (std::vector<Graphic*>::iterator iter = m_vGraphics.begin(); iter != m_vGraphics.end(); ++iter)
		(*iter)->Update();

	if (m_pCollider)
		m_pCollider->SetPos(m_ptPosition);
}


/**************************************************************/
// Render
//	- draw the entity's image at its position
/*virtual*/ void Entity::Render( void )
{
	// Verify the image
	//assert( m_hImage != SGD::INVALID_HANDLE && "Entity::Render - image was not set!" );
	
	SGD::Point drawPoint = m_ptPosition;
	drawPoint.Offset(-Game::GetInstance()->GetCamera());
	drawPoint.x = round(drawPoint.x);
	drawPoint.y = round(drawPoint.y);

	// Draw the image
	/*SGD::GraphicsManager::GetInstance()->DrawTexture( 
		m_hImage, drawPoint,
		m_fRotation, m_szSize/2 );*/

	// halfsize
	float size = 2.0f;

	for (std::vector<Graphic*>::iterator iter = m_vGraphics.begin(); iter != m_vGraphics.end(); ++iter)
		(*iter)->Render(drawPoint);

	drawPoint = SGD::Point(0, 0);
	drawPoint.Offset(-Game::GetInstance()->GetCamera());
	drawPoint.x = round(drawPoint.x);
	drawPoint.y = round(drawPoint.y);

	/*if (m_pCollider)
		m_pCollider->Render(drawPoint);*/

	//SGD::GraphicsManager::GetInstance()->DrawRectangle(SGD::Rectangle(drawPoint - SGD::Vector(size, size), drawPoint + SGD::Vector(size, size)), SGD::Color(255, 255, 255, 255));
}


/**************************************************************/
// GetRect
//	- calculate the entity's bounding rectangle
/*virtual*/ SGD::Rectangle Entity::GetRect( void ) const
{
	return SGD::Rectangle{ m_ptPosition, m_szSize };
}


/**************************************************************/
// HandleCollision
//	- respond to collision between entities
/*virtual*/ void Entity::HandleCollision( const IEntity* pOther )
{
	/* DO NOTHING */
	(void)pOther;		// unused parameter
}

void Entity::AddGraphic(Graphic* graphic)
{
	m_vGraphics.push_back(graphic);
}

Graphic* Entity::RemoveGraphic(Graphic* graphic)
{
	for (std::vector<Graphic*>::iterator iter = m_vGraphics.begin(); iter != m_vGraphics.end(); ++iter)
	{
		if (*iter == graphic)
		{
			m_vGraphics.erase(iter);
			return (*iter);
		}
	}

	return nullptr;
}

void Entity::AddTag(int tag)
{
	if (GetCollider())
		GetCollider()->AddTag(tag);
}

void Entity::SetCollider(Collider* collider)
{
	if (m_pCollider)
		delete m_pCollider;
	m_pCollider = collider;

	m_pCollider->SetEntity(this);

	m_pCollider->SetPos(m_ptPosition);
}

Collider* Entity::Collide(float x, float y, int tags[])
{
	if (m_pCollider)
		return m_pCollider->Collide(x, y, tags);
	return nullptr;
}

Collider* Entity::Collide(float x, float y, Collider* c)
{
	if (m_pCollider)
		return m_pCollider->Collide(x, y, c);
	return nullptr;
}

Collider* Entity::Collide(float x, float y, Entity* e)
{
	if (m_pCollider)
		return m_pCollider->Collide(x, y, e);
	return nullptr;
}


/**************************************************************/
// AddRef
//	- increase the reference count
/*virtual*/ void Entity::AddRef( void )
{
	assert( m_unRefCount != 0xFFFFFFFF && "Entity::AddRef - maximum reference count has been exceeded" );

	++m_unRefCount;
}


/**************************************************************/
// Release
//	- decrease the reference count
//	- self-destruct when the count is 0
/*virtual*/ void Entity::Release( void )
{
	--m_unRefCount;

	if( m_unRefCount == 0 )
		delete this;
}

void Entity::SetPosition(SGD::Point pos)
{
	m_ptPosition = pos;

	if (m_pCollider)
		m_pCollider->SetPos(m_ptPosition);
}