/***************************************************************
|	File:		OptionsState.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Options State
***************************************************************/

#pragma once

#include "TransState.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"

class Image;

class OptionsState :
	public TransState
{
public:
	static OptionsState* GetInstance();

	void Enter();
	void Exit();

	bool Input();
	void Update();
	void Render();

private:
	OptionsState() = default;
	~OptionsState() = default;

	OptionsState(const OptionsState&) = delete;
	OptionsState& operator= (const OptionsState&) = delete;

	SGD::HTexture m_hBackground = SGD::INVALID_HANDLE;
	Image* m_pBackground;
};

