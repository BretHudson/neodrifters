#pragma once

#include "IGameState.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"

class Image;
class Entity;
class EntityManager;

class TransState :
	public IGameState
{
public:
	TransState();
	~TransState();

	virtual void Enter();
	virtual void Exit();

	virtual bool Input() = 0;
	virtual void Update();
	virtual void Render();

protected:
	void FadeToState(IGameState* toState);
	EntityManager*			m_pEntities = nullptr;

//private:
	IGameState* m_pToState = nullptr;

	SGD::HTexture m_hTransition;
	Image* m_pTransitionImage;
	Entity* m_pTransitionEntity;
};

