/***************************************************************
|	File:		VehicleManager.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Vehicle Manager
***************************************************************/

#include "VehicleManager.h"
#include "Vehicle.h"

VehicleManager::VehicleManager()
{
	//
}

VehicleManager::~VehicleManager()
{
	for (std::vector<Vehicle*>::iterator iter = m_vVehicles.begin(); iter != m_vVehicles.end(); ++iter)
		(*iter)->Release();
}

void VehicleManager::AddVehicle(Vehicle* vehicle)
{
	m_vVehicles.push_back(vehicle);
}

Vehicle* VehicleManager::GetVehicle(unsigned int n) const
{
	if (n > m_vVehicles.size())
		return nullptr;
	return m_vVehicles[n];
}

int VehicleManager::GetNumVehicles()
{
	return m_vVehicles.size();
}