/***************************************************************
|	File:		Destructable.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A destructable class that the player can
|				destroy for points
***************************************************************/

#pragma once

#include "Entity.h"

class Image;

class Destructable :
	public Entity
{
public:
	Destructable(SGD::HTexture standing, int swidth, int sheight, SGD::HTexture hit, int hwidth, int hheight);
	~Destructable();

	void Update();

private:
	Image* m_pStanding;
	Image* m_pHit;

	bool m_bHit = false;
};
