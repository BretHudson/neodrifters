/***********************************************************************\
|																		|
|	File:			Image.cpp											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Updates and renders a non-animated image			|
|																		|
\***********************************************************************/

#include "Image.h"

#include <cmath>

Image::Image(SGD::HTexture texture, int width, int height)
{
	m_hTexture = texture;
	m_szSize.width = (float)width;
	m_szSize.height = (float)height;
	m_rtDrawRect = SGD::Rectangle(0, 0, (float)width, (float)height);
}

Image::~Image()
{
}

void Image::Update()
{
	//
}

void Image::Render(SGD::Point ptDrawPos)
{
	SGD::Vector offset = m_vtOffset;
	offset.x = int(offset.x) + ((GetScaleX() < 0) ? m_szSize.width : 0);
	offset.y = int(offset.y) + ((GetScaleY() < 0) ? m_szSize.height : 0);

	SGD::Vector origin = m_vtOrigin;
	origin.x = round(origin.x);
	origin.y = round(origin.y);

	SGD::GraphicsManager::GetInstance()->DrawTextureSection(m_hTexture, ptDrawPos + offset - origin, m_rtDrawRect, m_fRotation, origin, SGD::Color(m_iAlpha, 255, 255, 255), SGD::Size(m_ptScale.x, m_ptScale.y));
}

Graphic* Image::Clone()
{
	return new Image(*this);
}