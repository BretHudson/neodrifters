/***************************************************************
|	File:		InstructionsState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Instructions State
***************************************************************/

#include "InstructionsState.h"

#include "Image.h"

#include "Input.h"

#include "Game.h"
#include "MainMenuState.h"

InstructionsState* InstructionsState::GetInstance()
{
	static InstructionsState s_Instance;
	return &s_Instance;
}

void InstructionsState::Enter()
{
	TransState::Enter();
	m_hBackground = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/instructionsmenu/be_instructionsmenu.png");
	m_pBackground = new Image(m_hBackground, 960, 544);
}

void InstructionsState::Exit()
{
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hBackground);
	delete m_pBackground;
	TransState::Exit();
}

bool InstructionsState::Input()
{
	// Press Escape to return to Main Menu
	if (Input::CheckPressed("menuback"))
	{
		FadeToState(MainMenuState::GetInstance());

		return true;
	}

	return true;
}

void InstructionsState::Update()
{
	TransState::Update();
}

void InstructionsState::Render()
{
	m_pBackground->Render(SGD::Point(0, 0));
	TransState::Render();
}