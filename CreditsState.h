/***************************************************************
|	File:		CreditsState.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A IGameState that is the credits
***************************************************************/

#pragma once

#include "TransState.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"

class Image;

class CreditsState :
	public TransState
{
public:
	static CreditsState* GetInstance();

	void Enter();
	void Exit();

	bool Input();
	void Update();
	void Render();

private:
	CreditsState() = default;
	~CreditsState() = default;

	CreditsState(const CreditsState&) = delete;
	CreditsState& operator= (const CreditsState&) = delete;

	SGD::HTexture m_hBackground = SGD::INVALID_HANDLE;
	Image* m_pBackground;
};

