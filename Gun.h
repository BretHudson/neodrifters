/***************************************************************
|	File:		Gun.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	An entity that shoots projectiles (Bullet)
***************************************************************/

#pragma once
#include "Entity.h"
#include "Vehicle.h"
#include <string>

class Gun :
	public Entity
{
public:
	Gun();
	Gun(const Gun& other);
	~Gun();

	void Update();
	void Render();

	// Accessors
	std::string GetName(void) const { return m_sName; }
	unsigned int GetID(void) const { return m_iID; }
	int GetReload(void) const { return m_iReloadTimeout; }
	bool GetAlternate(void) const { return m_bAlternate; }
	bool GetAutomatic(void) const { return m_bAutomatic; }
	Graphic* GetImage(void) const { return m_vGraphics[0]; }

	// Mutators
	void SetName(std::string name) { m_sName = name; }
	void SetID(unsigned int id) { m_iID = id; }
	void SetReload(int reload) { m_iReloadTimeout = reload; }
	void SetAlternate(bool alternate) { m_bAlternate = alternate; }
	void SetAutomatic(bool automatic) { m_bAutomatic = automatic; }

	void SetOwner(Vehicle* owner) { m_pOwner = owner; }
	void SetSide(std::string side) { m_sSide = side; }


	bool IsAutomatic() { return m_bAutomatic; }
	bool Shoot(bool pressed, bool down);

private:
	Vehicle* m_pOwner;
	std::string m_sSide;

	std::string m_sName;
	int m_iID;

	int m_iReloadTimer;
	int m_iReloadTimeout;

	bool m_bAlternate;
	bool m_bAutomatic;
};