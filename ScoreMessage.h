/***************************************************************
|	File:		ScoreMessage.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Score Message
***************************************************************/

#pragma once

#include "..\SGD Wrappers\SGD_Message.h"

class ScoreMessage :
	public SGD::Message
{
public:
	ScoreMessage(int score);
	~ScoreMessage();

	int GetScore() const { return m_iScore; }

private:
	int m_iScore;
};
