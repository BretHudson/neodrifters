/***************************************************************
|	File:		Vehicle.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A base class for Vehicles
***************************************************************/

#pragma once
#include "Entity.h"

#include <string>

class Guns;
class PolygonCollider;

class Vehicle :
	public Entity
{
public:
	Vehicle();
	Vehicle(const Vehicle& other);
	~Vehicle();

	virtual void Update();

	virtual void Screenshake(float shake);
	
	void PositionGuns();
	virtual void Shoot(bool pressed, bool down);

	void SetName(std::string name) { m_sName = name; }
	void SetID(unsigned int id) { m_iID = id; }

	void SetMaxSpeed(float speed) { m_fMaxspeed = speed; }
	void SetIdleSpeed(float speed) { m_fIdleSpeed = speed; }
	void SetTurnAmount(float amount) { m_fTurnAmount = amount; }

	void SetRotation(float angle);

	std::string GetName(void) const { return m_sName; }
	float GetSpeed(void) const { return m_fSpeed; }
	unsigned int GetID(void) const { return m_iID; }
	Graphic* GetImage(void) const { return m_vGraphics[0]; }

	void SetGuns(Guns* guns);
	Guns* GetGuns(void) { return m_pGuns; }

	SGD::Point GetCenter() const;

protected:
	Guns* m_pGuns;

	std::string m_sName;
	unsigned int m_iID;

	float m_fSpeed = 0;
	float m_fMaxspeed = 0;
	float m_fIdleSpeed = 0;

	bool m_bEbrake = false;
	bool m_bBrake = false;
	bool m_bTakeoff = false;
	bool m_bGas = false;
	
	bool m_bShooting = false;

	float m_fTurnAmount = 0;
	float m_fAngleInDeg = 0;

	int m_iTurnDir = 0;
};
