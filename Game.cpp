/***************************************************************
|	File:		Game.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Game class controls the SGD wrappers
|				& runs the game state machine
***************************************************************/

#include "Game.h"

#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_String.h"

#include "../TinyXML/tinyxml.h"

#include "BitmapFont.h"
#include "IGameState.h"
#include "MainMenuState.h"
#include "CreditsState.h"

#include "Input.h"
#include "Util.h"

#include "GunManager.h"
#include "Gun.h"
#include "Bullet.h"

#include "VehicleManager.h"
#include "Vehicle.h"

#include "Image.h"
#include "Spritemap.h"
#include "Text.h"

#include <ctime>
#include <cstdlib>
#include <cassert>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <iostream>
#include <ostream>
#include <streambuf>

/**************************************************************/
// Singleton
//	- instantiate the static member
/*static*/ Game* Game::s_pInstance = nullptr;
/*static*/ GunManager* Game::s_pGunManager = nullptr;
/*static*/ VehicleManager* Game::s_pVehicleManager = nullptr;

// GetInstance
//	- allocate the ONE instance & return it
/*static*/ Game* Game::GetInstance(void)
{
	if (s_pInstance == nullptr)
		s_pInstance = new Game;

	return s_pInstance;
}

// DeleteInstance
//	- deallocate the ONE instance
/*static*/ void Game::DeleteInstance(void)
{
	delete s_pInstance;
	s_pInstance = nullptr;
}


/*static*/ GunManager* Game::GetGunManager(void)
{
	return s_pGunManager;
}


/*static*/ VehicleManager* Game::GetVehicleManager(void)
{
	return s_pVehicleManager;
}


/**************************************************************/
// Initialize
//	- initialize the SGD wrappers
//	- enter the Main Menu state
bool Game::Initialize(float width, float height, float frameRate)
{
	// Seed First!
	srand((unsigned int)time(nullptr));
	rand();

	Game::GetInstance()->frameRate = 1.0f / frameRate;

	// Initialize the wrappers
	if (SGD::AudioManager::GetInstance()->Initialize() == false
		|| SGD::GraphicsManager::GetInstance()->Initialize(false) == false
		|| SGD::InputManager::GetInstance()->Initialize() == false)
		return false;

	// Store the size parameters
	m_fScreenWidth = width;
	m_fScreenHeight = height;


	// Allocate & initialize the font
	m_pFont = new BitmapFont;
	m_pFont->Initialize();

	m_pWonText = new Text("You won! Press Enter", 20, 300);
	m_pLoseText = new Text("You Lost! Press Enter", 20, 300);

	m_hWon = SGD::AudioManager::GetInstance()->LoadAudio("resource/audio/be_player_win.wav");
	m_hLose = SGD::AudioManager::GetInstance()->LoadAudio("resource/audio/be_player_lose.wav");


	// Start the game in the Main Menu state
	ChangeState(MainMenuState::GetInstance());


	// Store the current time (in milliseconds)
	m_ulGameTime = GetTickCount();


	// Set up the controls for the game
	InitControls();


	// Load the variables from XML
	LoadXML();


	// Enable the debug console
	//GetInstance()->EnableConsole();


	return true;	// success!
}

void Game::InitControls()
{
	// Menu controls - Keyboard
	Input::RegisterKeys("menuup", { SGD::Key::Up, SGD::Key::W });
	Input::RegisterKeys("menudown", { SGD::Key::Down, SGD::Key::S });
	Input::RegisterKey("menuselect", SGD::Key::Enter);
	Input::RegisterKey("menuback", SGD::Key::Escape);
	Input::RegisterKey("alt", SGD::Key::Alt);
	Input::RegisterKey("pause", SGD::Key::Escape);
	Input::RegisterKey("pause", SGD::Key::P);

	// Menu controls - Controller
	Input::RegisterButton("pause", 9);
	Input::RegisterButton("menuselect", 2);
	Input::RegisterButton("menuback", 1);
	Input::RegisterAxis("menuup", "menudown", AxisTypes::LEFTY);

	// Player controls - Keyboard
	Input::RegisterKey("left", SGD::Key::Left);
	Input::RegisterKey("left", SGD::Key::A);
	Input::RegisterKey("right", SGD::Key::Right);
	Input::RegisterKey("right", SGD::Key::D);
	Input::RegisterKey("gas", SGD::Key::Up);
	Input::RegisterKey("gas", SGD::Key::W);
	Input::RegisterKey("brake", SGD::Key::Down);
	Input::RegisterKey("brake", SGD::Key::S);
	Input::RegisterKey("ebrake", SGD::Key::Control);
	Input::RegisterKey("shoot", SGD::Key::Space);

	// Player controls - Controller
	Input::RegisterButton("gas", 2);
	Input::RegisterButton("ebrake", 1);
	Input::RegisterButton("shoot", 3);

	// TODO: PAUSE
}


void Game::LoadXML()
{
	TiXmlDocument carDoc, gunDoc;
	TiXmlElement* carRoot;
	TiXmlElement* gunRoot;

	// Load cars
	if (s_pVehicleManager)	delete s_pVehicleManager;
	s_pVehicleManager = new VehicleManager();

	carDoc.LoadFile("resource/xml/vehicles.xml");

	carRoot = carDoc.RootElement();
	if (carRoot == nullptr) return;

	TiXmlElement* currentVehicle = carRoot->FirstChildElement("Vehicle");
	while (currentVehicle)
	{
		CreateVehicle(currentVehicle);
		currentVehicle = currentVehicle->NextSiblingElement("Vehicle");
	}

	// Load guns (and bullets)
	if (s_pGunManager)	delete s_pGunManager;
	s_pGunManager = new GunManager();

	gunDoc.LoadFile("resource/xml/guns.xml");

	gunRoot = gunDoc.RootElement();
	if (gunRoot == nullptr) return;

	TiXmlElement* currentGun = gunRoot->FirstChildElement("Gun");
	while (currentGun)
	{
		CreateGun(currentGun);
		currentGun = currentGun->NextSiblingElement("Gun");
	}
}

void Game::CreateGun(TiXmlElement* currentGun)
{
	Gun* gun = new Gun();

	const char* name;
	int reload;
	int alternate;
	int automatic;

	int width;
	int height;

	name = currentGun->Attribute("name");
	currentGun->Attribute("reload", &reload);
	currentGun->Attribute("alternate", &alternate);
	currentGun->Attribute("automatic", &automatic);

	currentGun->Attribute("width", &width);
	currentGun->Attribute("height", &height);

	gun->SetName(name);
	gun->SetReload(reload);
	gun->SetAlternate(!!alternate);
	gun->SetAutomatic(!!automatic);

	gun->SetSize(SGD::Size((float)width, (float)height));

	std::string textureFilename = "resource/graphics/weapons/";
	textureFilename += currentGun->Attribute("texture");
	SGD::HTexture gunTexture = SGD::GraphicsManager::GetInstance()->LoadTexture(textureFilename.c_str());
	Image* gunImage = new Image(gunTexture, width, height);
	gunImage->CenterOrigin();
	gun->AddGraphic(gunImage);

	m_vGunTextures.push_back(gunTexture);

	TiXmlElement* currentBullet = currentGun->FirstChildElement("Bullet");

	while (currentBullet)
	{
		CreateBullet(currentBullet, s_pGunManager->GetNumGuns());
		currentBullet = currentBullet->NextSiblingElement("Bullet");
	}

	gun->SetID(s_pGunManager->GetNumGuns());

	s_pGunManager->AddGun(gun);
}

void Game::CreateBullet(TiXmlElement* currentBullet, int numGuns)
{
	Bullet* bullet = new Bullet();

	// Load stuff
	int width;
	int height;

	int explosive;
	int speed;
	int life;
	int homing;
	int damage;

	int animated;

	currentBullet->Attribute("width", &width);
	currentBullet->Attribute("height", &height);
	currentBullet->Attribute("explosive", &explosive);
	currentBullet->Attribute("speed", &speed);
	currentBullet->Attribute("life", &life);
	currentBullet->Attribute("homing", &homing);
	currentBullet->Attribute("damage", &damage);
	currentBullet->Attribute("animated", &animated);

	bullet->SetExplosive(!!explosive);
	bullet->SetSpeed((float)speed);
	bullet->SetLife(life);
	bullet->SetHoming(!!homing);
	bullet->SetDamage((float)damage);

	// Assign images to the thing
	std::string textureFilename = "resource/graphics/weapons/";
	textureFilename += currentBullet->Attribute("texture");
	SGD::HTexture bulletTexture = SGD::GraphicsManager::GetInstance()->LoadTexture(textureFilename.c_str());

	if (!!animated)
	{
		Spritemap* bulletSprite = new Spritemap(bulletTexture, width, height);
		bulletSprite->CenterOrigin();
		bulletSprite->Add("name", 0, 1, 2, 3, -1);
		bulletSprite->SetFramerate("name", 4);
		bulletSprite->SetColumnsRows(4, 1);
		bulletSprite->Play("name");
		bullet->AddGraphic(bulletSprite);
	}
	else
	{
		Image* bulletImage = new Image(bulletTexture, width, height);
		bulletImage->CenterOrigin();
		bullet->AddGraphic(bulletImage);
	}

	m_vBulletTextures.push_back(bulletTexture);

	// Add to container

	s_pGunManager->AddBullet(bullet, numGuns);
}

void Game::CreateVehicle(TiXmlElement* currentVehicle)
{
	Vehicle* vehicle = new Vehicle();

	const char* name;
	int maxspeed;
	int idlespeed;
	int turnamount;

	int width;
	int height;
	int originx;
	int originy;

	name = currentVehicle->Attribute("name");
	currentVehicle->Attribute("maxspeed", &maxspeed);
	currentVehicle->Attribute("idlespeed", &idlespeed);
	currentVehicle->Attribute("turnamount", &turnamount);

	currentVehicle->Attribute("width", &width);
	currentVehicle->Attribute("height", &height);
	currentVehicle->Attribute("originx", &originx);
	currentVehicle->Attribute("originy", &originy);

	vehicle->SetName(name);
	vehicle->SetMaxSpeed((float)maxspeed);
	vehicle->SetIdleSpeed((float)idlespeed);
	vehicle->SetTurnAmount((float)turnamount);

	vehicle->SetSize(SGD::Size((float)width, (float)height));

	std::string textureFilename = "resource/graphics/vehicles/";
	textureFilename += currentVehicle->Attribute("texture");
	SGD::HTexture vehicleTexture = SGD::GraphicsManager::GetInstance()->LoadTexture(textureFilename.c_str());
	Image* vehicleImage = new Image(vehicleTexture, width, height);
	vehicleImage->CenterOrigin();
	vehicleImage->SetOrigin((float)originx, (float)originy);
	vehicle->AddGraphic(vehicleImage);

	m_vVehicleTextures.push_back(vehicleTexture);

	vehicle->SetID(s_pVehicleManager->GetNumVehicles());
	s_pVehicleManager->AddVehicle(vehicle);
}


/**************************************************************/
// Main
//	- update the SGD wrappers
//	- run the current state
int Game::Update(void)
{
	// Calculate the elapsed time between frames
	unsigned long now = GetTickCount();					// current time in milliseconds
	elapsedTime += (now - m_ulGameTime) / 1000.0f;	// convert to fraction of a second
	m_ulGameTime = now;									// store the current time

	// Cap the elapsed time to 1/8th of a second
	/*if( elapsedTime > 0.125f )
		elapsedTime = 0.125f;*/

	// Update & render the current state
	if (elapsedTime > frameRate)
	{
		// Update the wrappers
		if (SGD::AudioManager::GetInstance()->Update() == false
			|| SGD::GraphicsManager::GetInstance()->Update() == false
			|| SGD::InputManager::GetInstance()->Update() == false)
			return -10;		// exit FAILURE!

		if ((Input::CheckDown("alt")) && (Input::CheckPressed("menuselect")))
		{
			SGD::GraphicsManager::GetInstance()->Resize(SGD::Size(m_fScreenWidth, m_fScreenHeight), m_bFullscreen);
			m_bFullscreen = !m_bFullscreen;
			return 0;
		}

		// Let the current state handle input
		if (m_pCurrState->Input() == false)
			return 1;	// exit success!

		if (!m_bLose)
			m_pCurrState->Update();

		SGD::Vector shakeOffset = { round(Util::Random(shakeAmount)), round(Util::Random(shakeAmount)) };
		SGD::Vector originalCamera = m_vtCameraOffset;

		m_vtCameraOffset += shakeOffset;
		m_pCurrState->Render();
		m_vtCameraOffset = originalCamera;

		if (m_bWon)
		{
			if (Input::CheckPressed("menuselect"))
			{
				Game::GetInstance()->ChangeState(CreditsState::GetInstance());
				m_bWon = false;
			}
			m_pWonText->Render(SGD::Point(0, 0));
		}

		if (m_bLose)
		{
			if (Input::CheckPressed("menuselect"))
			{
				Game::GetInstance()->ChangeState(CreditsState::GetInstance());
				m_bLose = false;
			}
			m_pLoseText->Render(SGD::Point(0, 0));
		}

		if (shakeAmount > 0)	shakeAmount -= 0.25f;
		if (shakeAmount > 5)	shakeAmount *= 0.95f;

		// Decrease the elapsedTime 
		if (elapsedTime > frameRate)
		{
			elapsedTime -= frameRate;

			if (elapsedTime > frameRate)
				elapsedTime = frameRate;
		}
	}
	return 0;		// keep playing!
}


/**************************************************************/
// Terminate
//	- exit the current state
//	- terminate the SGD wrappers
void Game::Terminate(void)
{
	// Exit the current state
	ChangeState(nullptr);


	// Terminate & deallocate the font
	m_pFont->Terminate();
	delete m_pFont;
	m_pFont = nullptr;

	delete m_pWonText;
	delete m_pLoseText;

	if (s_pGunManager)
		delete s_pGunManager;
	s_pGunManager = nullptr;

	if (s_pVehicleManager)
		delete s_pVehicleManager;
	s_pVehicleManager = nullptr;

	SGD::AudioManager::GetInstance()->UnloadAudio(m_hWon);
	SGD::AudioManager::GetInstance()->UnloadAudio(m_hLose);

	// Unload all the graphics for guns and vehicles
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	for (std::vector<SGD::HTexture>::iterator iter = m_vGunTextures.begin(); iter != m_vGunTextures.end(); ++iter)
		pGraphics->UnloadTexture(*iter);

	for (std::vector<SGD::HTexture>::iterator iter = m_vBulletTextures.begin(); iter != m_vBulletTextures.end(); ++iter)
		pGraphics->UnloadTexture(*iter);

	for (std::vector<SGD::HTexture>::iterator iter = m_vVehicleTextures.begin(); iter != m_vVehicleTextures.end(); ++iter)
		pGraphics->UnloadTexture(*iter);


	// Terminate the core SGD wrappers
	SGD::AudioManager::GetInstance()->Terminate();
	SGD::AudioManager::DeleteInstance();

	SGD::GraphicsManager::GetInstance()->Terminate();
	SGD::GraphicsManager::DeleteInstance();

	SGD::InputManager::GetInstance()->Terminate();
	SGD::InputManager::DeleteInstance();
}


void Game::SetCamera(SGD::Point point)
{
	SGD::Vector offset;
	offset.x = (int)point.x - m_fScreenWidth / 2;
	offset.y = (int)point.y - m_fScreenHeight / 2;
	SetCamera(offset);
}

void Game::Won()
{
	m_bWon = true;
	SGD::AudioManager::GetInstance()->PlayAudio(m_hWon);
}

void Game::Lose()
{
	m_bLose = true;
	SGD::AudioManager::GetInstance()->PlayAudio(m_hLose);
}


/**************************************************************/
// ChangeState
//	- exit the current state to release resources
//	- enter the new state to allocate resources
//	- DANGER! Exiting the current state can CRASH the program!
//	  The state can ONLY be exited from the
//	  Input, Update, and Render methods!!!
void Game::ChangeState(IGameState* pNewState)
{
	// Exit the old state
	if (m_pCurrState != nullptr)
		m_pCurrState->Exit();

	// Store the new state
	m_pCurrState = pNewState;

	// Enter the new state
	if (m_pCurrState != nullptr)
		m_pCurrState->Enter();
}