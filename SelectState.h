/***************************************************************
|	File:		SelectState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Select State
***************************************************************/

#pragma once

#include "TransState.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"

#include <vector>
#include <list>

class EntityManager;
class MenuCircle;
class Image;

class SelectState :
	public TransState
{
public:
	static SelectState* GetInstance();

	void Enter();
	void Exit();

	bool Input();
	void Update();
	void Render();

private:
	SelectState() = default;
	~SelectState() = default;

	SelectState(const SelectState&) = delete;
	SelectState& operator= (const SelectState&) = delete;

	std::vector<SGD::HTexture> m_vTextures;

	// Cursor stuffs
	bool locked = true;

	int menu = 0;

	int angle = 0;
	int turnAmount = 2;

	float carRotation = 0.0f;

	int numVehicles = 0, numGuns = 0;
	int vehicleID = 0, leftGunID = 0, rightGunID = 0;

	std::list<MenuCircle*> m_lMenuCircles;

	Image* innerCircle;

	int dir;
};