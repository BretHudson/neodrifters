/***************************************************************
|	File:		MenuCircleThree.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle Three
***************************************************************/

#pragma once
#include "MenuCircle.h"

class MenuCircleThree :
	public MenuCircle
{
public:
	MenuCircleThree(SGD::HTexture bright, SGD::HTexture dim);
	~MenuCircleThree();

	void Update();
	void Render();
};
