/***************************************************************
|	File:		DamageMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Damage Message
***************************************************************/

#include "DamageMessage.h"

#include "MessageID.h"

#include "Entity.h"

DamageMessage::DamageMessage(Entity* entity, int damage) : Message(MessageID::MSG_DAMAGE)
{
	m_pEntity = entity;
	entity->AddRef();
	m_iDamage = damage;
}

DamageMessage::~DamageMessage()
{
	m_pEntity->Release();
}