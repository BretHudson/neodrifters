/***************************************************************
|	File:		SelectState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Select State
***************************************************************/

#include "SelectState.h"

#include "GameplayState.h"
#include "MainMenuState.h"

#include "Game.h"
#include "Input.h"
#include "GunManager.h"
#include "VehicleManager.h"

#include "EntityManager.h"

#include "Util.h"

#include "Image.h"

#include "MenuCircleOne.h"
#include "MenuCircleTwo.h"
#include "MenuCircleThree.h"

SelectState* SelectState::GetInstance()
{
	static SelectState s_Instance;
	return &s_Instance;
}

void SelectState::Enter()
{
	TransState::Enter();

	Game::GetInstance()->SetCamera(SGD::Vector(0, 0));

	locked = true;
	angle = 0;
	menu = 0;
	carRotation = 0.0f;

	numVehicles = Game::GetInstance()->GetVehicleManager()->GetNumVehicles();
	numGuns = Game::GetInstance()->GetGunManager()->GetNumGuns();

	// Load the textures
	SGD::HTexture circle1Bright = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/circle 1/be_circle1_bright.png");
	SGD::HTexture circle1Dim = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/circle 1/be_circle1_dim.png");

	SGD::HTexture circle2Bright = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/circle 2/be_circle2_bright.png");
	SGD::HTexture circle2Dim = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/circle 2/be_circle2_dim.png");

	SGD::HTexture circle3Bright = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/circle 3/be_circle3_bright.png");
	SGD::HTexture circle3Dim = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/circle 3/be_circle3_dim.png");

	SGD::HTexture innerCircleTexture = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/be_innercircle.png");
	innerCircle = new Image(innerCircleTexture, 598, 598);
	innerCircle->CenterOrigin();

	// Create the circles
	MenuCircle* circle1 = new MenuCircleOne(circle1Bright, circle1Dim, &vehicleID, &leftGunID, &rightGunID);
	m_lMenuCircles.push_back(circle1);
	m_pEntities->AddEntity(circle1, 0);
	
	MenuCircle* circle2 = new MenuCircleTwo(circle2Bright, circle2Dim);
	m_lMenuCircles.push_back(circle2);
	m_pEntities->AddEntity(circle2, 0);

	MenuCircle* circle3 = new MenuCircleThree(circle3Bright, circle3Dim);
	m_lMenuCircles.push_back(circle3);
	m_pEntities->AddEntity(circle3, 0);

	// Add the textures to the texture vector
	m_vTextures.push_back(circle1Bright);
	m_vTextures.push_back(circle1Dim);
	m_vTextures.push_back(circle2Bright);
	m_vTextures.push_back(circle2Dim);
	m_vTextures.push_back(circle3Bright);
	m_vTextures.push_back(circle3Dim);
	m_vTextures.push_back(innerCircleTexture);
}

void SelectState::Exit()
{
	for (std::list<MenuCircle*>::iterator iter = m_lMenuCircles.begin(); iter != m_lMenuCircles.end(); ++iter)
		(*iter)->Release();
	m_lMenuCircles.clear();

	for (std::vector<SGD::HTexture>::iterator iter = m_vTextures.begin(); iter != m_vTextures.end(); ++iter)
		SGD::GraphicsManager::GetInstance()->UnloadTexture(*iter);

	delete innerCircle;

	TransState::Exit();
}

bool SelectState::Input()
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	// Press Escape to return to Main Menu
	if (Input::CheckPressed("menuback"))
	{
		FadeToState( MainMenuState::GetInstance() );

		return true;
	}

	if (Input::CheckPressed("menuselect"))
	{
		GameplayState::GetInstance()->SetPlayerVars(vehicleID, leftGunID, rightGunID);
		FadeToState(GameplayState::GetInstance());
		return true;
	}

	return true;
}

void SelectState::Update()
{
	for (std::list<MenuCircle*>::iterator iter = m_lMenuCircles.begin(); iter != m_lMenuCircles.end(); ++iter)
		(*iter)->SetLocked(locked);

	if (locked)
	{
		dir = int(Input::CheckPressed("right")) - int(Input::CheckPressed("left"));
		if (dir) locked = false;
	}

	if (!locked)
	{
		angle += turnAmount * dir;
		angle = (angle + 360) % 360;
		if (angle % 120 == 0)
			locked = true;
	}

	int i = 0;
	for (std::list<MenuCircle*>::iterator iter = m_lMenuCircles.begin(); iter != m_lMenuCircles.end(); ++iter, ++i)
	{
		float newAngle = (float)((i * 120 + angle) % 360);
		if (newAngle != 0)
			newAngle = newAngle / 180.0f * 3.14159f;
		(*iter)->SetRotation(newAngle);
	}

	m_pEntities->UpdateAll();

	vehicleID = Util::CircleClamp(vehicleID, 0, numVehicles - 1);
	leftGunID = Util::CircleClamp(leftGunID, 0, numGuns - 1);
	rightGunID = Util::CircleClamp(rightGunID, 0, numGuns - 1);

	TransState::Update();
}

void SelectState::Render()
{
	innerCircle->Render(SGD::Point(480, 32));

	TransState::Render();

	/*for (std::vector<Text*>::iterator iter = texts.begin(); iter != texts.end(); ++iter)
		(*iter)->Render(SGD::Point(0, 0));*/
}