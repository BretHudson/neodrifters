/***************************************************************
|	File:		CreateBulletMessage.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Create Bullet Message
***************************************************************/

#pragma once

#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Message.h"

class Vehicle;

class CreateBulletMessage : public SGD::Message
{
public:
	CreateBulletMessage(SGD::Point point, unsigned int gunID, Vehicle* vehicle);
	~CreateBulletMessage();
	
	SGD::Point GetSpawnPoint(void) const { return m_ptSpawn; }
	unsigned int GetGunID(void) const { return m_iGunID; }
	Vehicle* GetVehicle(void) const { return m_pVehicle; }

private:
	SGD::Point m_ptSpawn;
	unsigned int m_iGunID;
	Vehicle* m_pVehicle;
};