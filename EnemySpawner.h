/***************************************************************
|	File:		EnemySpawner.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	AKA Portal, spawns Enemy objects
***************************************************************/

#pragma once
#include "Entity.h"
class EnemySpawner :
	public Entity
{
public:
	EnemySpawner(int x, int y, int id, SGD::HTexture texture);
	~EnemySpawner();

	int	GetType(void) const override { return ENT_SPAWNER; }

	void Update();

	void Spawn();

	int GetID() const { return m_iID; }
	void SetID(int id) { m_iID = id; }

private:
	int m_iID;

	int m_iTimer = 0;
	int m_iTimeout = 740;
};

