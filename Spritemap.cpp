/***********************************************************************\
|																		|
|	File:			Spritemap.cpp										|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Updates and renders an animated image				|
|																		|
\***********************************************************************/

#include "Spritemap.h"

#include <cstdarg>
#include <vector>

Spritemap::Spritemap(SGD::HTexture texture, int width, int height) : Image(texture, width, height)
{
	//
}

Spritemap::~Spritemap()
{
	for (std::map<std::string, Anim*>::iterator iter = m_vAnimations.begin(); iter != m_vAnimations.end(); ++iter)
		delete iter->second;
}

void Spritemap::Update()
{
	if (m_pCurrentAnim)
		m_pCurrentAnim->Update();
}

void Spritemap::Render(SGD::Point ptDrawPos)
{
	SGD::Vector offset = m_vtOffset;
	offset.x = int(offset.x) + ((GetScaleX() < 0) ? m_szSize.width : 0);
	offset.y = int(offset.y) + ((GetScaleY() < 0) ? m_szSize.height : 0);

	SGD::Vector origin = m_vtOrigin;
	origin.x = round(origin.x);
	origin.y = round(origin.y);

	float left = m_szSize.width * (m_pCurrentAnim->GetFrame() % m_iRows);
	float top = m_szSize.height * (m_pCurrentAnim->GetFrame() / m_iRows);
	m_rtDrawRect = SGD::Rectangle(left, top, left + m_szSize.width, top + m_szSize.height);

	SGD::GraphicsManager::GetInstance()->DrawTextureSection(m_hTexture, ptDrawPos + offset - origin, m_rtDrawRect, m_fRotation, origin, SGD::Color(m_iAlpha, 255, 255, 255), SGD::Size(m_ptScale.x, m_ptScale.y));
}

Graphic* Spritemap::Clone()
{
	Spritemap* clone = new Spritemap(*this);

	// Duplicate the Anims
	Anim* currentAnim = nullptr;
	
	clone->m_vAnimations.clear();

	for (std::map<std::string, Anim*>::iterator iter = m_vAnimations.begin(); iter != m_vAnimations.end(); ++iter)
	{
		Anim* copy = new Anim(*iter->second);
		Anim* second = iter->second;
		clone->m_vAnimations.insert(std::pair<std::string, Anim*>(iter->first, copy));

		if (iter->second == m_pCurrentAnim)
			currentAnim = copy;
	}

	clone->m_pCurrentAnim = currentAnim;

	return clone;
}

void Spritemap::Add(std::string name, int frames, ...)
{
	Anim* anim = new Anim();

	va_list list;

	va_start(list, frames);

	while (frames != -1)
	{
		anim->AddFrame(frames);
		frames = va_arg(list, int);
	}

	va_end(list);

	m_vAnimations.insert(std::pair<std::string, Anim*>(name, anim));
}

void Spritemap::SetFramerate(std::string name, int framerate)
{
	for (std::map<std::string, Anim*>::iterator iter = m_vAnimations.begin(); iter != m_vAnimations.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			(*iter).second->SetFramerate(framerate);
			break;
		}
	}
}

void Spritemap::Play(std::string name)
{
	for (std::map<std::string, Anim*>::iterator iter = m_vAnimations.begin(); iter != m_vAnimations.end(); ++iter)
	{
		if ((*iter).first == name)
		{
			m_pCurrentAnim = (*iter).second;
			break;
		}
	}
}