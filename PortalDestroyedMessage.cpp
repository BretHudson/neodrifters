/***************************************************************
|	File:		PortalDestroyedMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Portal Destroyed Message
***************************************************************/

#include "PortalDestroyedMessage.h"

#include "MessageID.h"

#include "Entity.h"

PortalDestroyedMessage::PortalDestroyedMessage(Entity* entity) : Message(MessageID::MSG_PORTAL_DESTROYED)
{
	m_pEntity = entity;
	m_pEntity->AddRef();
}

PortalDestroyedMessage::~PortalDestroyedMessage()
{
	m_pEntity->Release();
}
