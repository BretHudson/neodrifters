/***************************************************************
|	File:		GameplayState.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	GameplayState class initializes & runs the game logic
***************************************************************/

#ifndef GAMEPLAYSTATE_H
#define GAMEPLAYSTATE_H


#include "TransState.h"						// uses BaseGameState
#include "../SGD Wrappers/SGD_Handle.h"			// uses HTexture & HAudio
#include "../SGD Wrappers/SGD_Declarations.h"	// uses Message



/**************************************************************/
// Forward class declaration
//	- tells the compiler that the type exists
//	- allows us to create pointers or references
class Entity;
class EntityManager;

class Text;



/**************************************************************/
// GameplayState class
//	- runs the game logic
//	- SINGLETON! (Static allocation, not dynamic)
class GameplayState : public TransState
{
public:
	/**********************************************************/
	// Singleton Accessor:
	static GameplayState* GetInstance( void );

	
	/**********************************************************/
	// IGameState Interface:
	virtual void	Enter	( void )				override;	// load resources
	virtual void	Exit	( void )				override;	// unload resources
													
	virtual bool	Input	( void )				override;	// handle user input
	virtual void	Update	( void )	override;	// update game entities / animations
	virtual void	Render	( void )				override;	// render game entities / menus


	void SetPlayerVars(int veh, int lgun, int rgun);

	EntityManager* GetEntityManager() const { return m_pEntities; }


private:
	/**********************************************************/
	// SINGLETON (not-dynamically allocated)
	GameplayState( void )			= default;	// default constructor
	virtual ~GameplayState( void )	= default;	// destructor

	GameplayState( const GameplayState& )				= delete;	// copy constructor
	GameplayState& operator= ( const GameplayState& )	= delete;	// assignment operator

	
	/**********************************************************/
	// Game Entities
		
	
	Text* m_pScoreText;
	Text* m_pPortalText;
	int m_iScore = 0;
	int m_iPortals = 4;

	/**********************************************************/	
	// Message Callback Function:
	static void MessageProc( const SGD::Message* pMsg );


	Entity* CreatePlayer(int vehicleID, int gunLeftID, int gunRightID);
	Entity* CreateEnemy(int vehicleID, int gunLeftID, int gunRightID, int behavior);
	Entity* CreateTank();

	void FixEntityPlacement();
	void AddSpawners();

	Entity* m_pPlayer;
	int m_iVehicleID = 0, m_iLeftGunID = 0, m_iRightGunID = 0;

	SGD::HTexture m_hBackground = SGD::INVALID_HANDLE;
	SGD::HTexture m_hSpawner = SGD::INVALID_HANDLE;

	SGD::HTexture m_hTankBody = SGD::INVALID_HANDLE;
	SGD::HTexture m_hTankTurrets = SGD::INVALID_HANDLE;
	SGD::HTexture m_hTankMainGun = SGD::INVALID_HANDLE;

	SGD::HAudio m_hBackgroundMusic = SGD::INVALID_HANDLE;

	bool m_bPaused;

	int m_iPausedSelect = 0;

	Text* m_pPausedText;
	Text* m_pPausedText1;
	Text* m_pPausedText2;
	Text* m_pTimerText;

	float m_fTime = 36000;

	SGD::HAudio m_hHit = SGD::INVALID_HANDLE;
};

#endif //GAMEPLAYSTATE_H
