/***********************************************************************\
|																		|
|	File:			Spritemap.h											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Updates and renders an animated image				|
|																		|
\***********************************************************************/

#pragma once

#include "Image.h"
#include "Anim.h"

#include <string>
#include <map>

class Spritemap : public Image
{
public:
	Spritemap(SGD::HTexture texture, int width, int height);
	Spritemap(SGD::HTexture texture, SGD::Size size) : Spritemap(texture, int(size.width), int(size.height)) { };
	~Spritemap();

	void Update();
	void Render(SGD::Point ptDrawPos);
	Graphic* Clone();

	void Add(std::string name, int frames, ...);
	void SetFramerate(std::string name, int framerate);
	void SetColumnsRows(int columns, int rows) { m_iColumns = columns; m_iRows = rows; }
	void Play(std::string name);

private:
	std::map<std::string, Anim*> m_vAnimations;
	Anim* m_pCurrentAnim = nullptr;

	int m_iRows = 1;
	int m_iColumns = 1;
};

