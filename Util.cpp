/***********************************************************************\
|																		|
|	File:			Util.cpp											|
|	Author:			Bret Hudson										|
|	Purpose:		Holds useful math functions to speed up development |
|																		|
\***********************************************************************/

#include "Util.h"

#include <iostream>