/***********************************************************************\
|																		|
|	File:			Graphic.h											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		An abstract base class for any type of graphic		|
|																		|
\***********************************************************************/

#pragma once

#include "../SGD Wrappers/SGD_Geometry.h"

class Graphic
{
public:

	virtual ~Graphic() = 0;

	virtual void Update() = 0;
	virtual void Render(SGD::Point ptDrawPos) = 0;

	virtual Graphic* Clone() = 0;

	// Accessors
	SGD::Vector GetOffset() const { return m_vtOffset; }
	float GetOffsetX() const { return m_vtOffset.x; }
	float GetOffsetY() const { return m_vtOffset.y; }

	SGD::Vector GetOrigin() const { return m_vtOrigin; }
	float GetOriginX() const { return m_vtOrigin.x; }
	float SetOriginY() const { return m_vtOrigin.y; }

	SGD::Point GetScale() const { return m_ptScale; }
	float GetScaleX() const { return m_ptScale.x; }
	float GetScaleY() const { return m_ptScale.y; }

	SGD::Size GetSize() const { return m_szSize; }
	float GetWidth() const { return m_szSize.width; }
	float GetHeight() const { return m_szSize.height; }

	float GetRotation() const { return m_fRotation; }
	float GetAngle() const { return m_fRotation; }
	int GetAlpha() const { return m_iAlpha; }

	// Mutators
	void SetOffset(SGD::Vector offset) { m_vtOffset = offset; }
	void SetOffset(float x, float y) { m_vtOffset.x = x; m_vtOffset.y = y; }
	void SetOffsetX(float x) { m_vtOffset.x = x; }
	void SetOffsetY(float y) { m_vtOffset.y = y; }

	void CenterOrigin() { m_vtOrigin.x = m_szSize.width * 0.5f; m_vtOrigin.y = m_szSize.height * 0.5f; };
	void SetOrigin(SGD::Vector origin) { m_vtOrigin = origin; }
	void SetOrigin(float x, float y) { m_vtOrigin.x = x; m_vtOrigin.y = y; }
	void SetOriginX(float x) { m_vtOrigin.x = x; }
	void SetOriginY(float y) { m_vtOrigin.y = y; }

	void SetScale(SGD::Point scale) { m_ptScale = scale; }
	void SetScale(float scale) { m_ptScale.x = m_ptScale.y = scale; }
	void SetScaleX(float x) { m_ptScale.x = x; }
	void SetScaleY(float y) { m_ptScale.y = y; }

	void SetRotation(float angle) { m_fRotation = angle; }
	void SetAngle(float angle) { m_fRotation = angle; }
	void SetAlpha(int alpha) { m_iAlpha = alpha; }

protected:
	SGD::Vector m_vtOffset = SGD::Vector(0, 0);
	SGD::Vector m_vtOrigin = SGD::Vector(0, 0);
	SGD::Point m_ptScale = SGD::Point(1, 1);
	SGD::Size m_szSize = SGD::Size(0, 0);
	float m_fRotation = 0.0f;
	int m_iAlpha = 255;
};

inline Graphic::~Graphic()
{
	//
}