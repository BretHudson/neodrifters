/***********************************************************************\
|																		|
|	File:			PolygonCollider.h									|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		A class that hollds information for a polygon		|
|					collider and does appropriate functions in finding	|
|					projections and such								|
|																		|
\***********************************************************************/

#pragma once

#include "Collider.h"

#include <vector>

//typedef SGD::Vector Axis;

class Projection : public SGD::Vector
{
public:
	Projection() : Projection(0.0f, 0.0f) { };
	Projection(float X, float Y) : Vector(X, Y) { }

	bool Overlap(Projection p);

	float GetMin() const { return x; }
	float GetMax() const { return y; }
};

class Axis : public SGD::Vector
{
public:
	Axis() : Axis(0.0f, 0.0f) { };
	Axis(float X, float Y) : Vector(X, Y) { };
	Axis(SGD::Vector v1, SGD::Vector v2);
};

class PolygonCollider :
	public Collider
{
public:
	PolygonCollider();
	PolygonCollider(const PolygonCollider& other) = default;
	~PolygonCollider();

	void Render(SGD::Point drawPoint);

	void GenerateAxes();

	// Accessors
	std::vector<Axis> GetAxes();
	
	// Mutators
	void SetPoints(std::vector<SGD::Vector> points);

	virtual void SetPos(SGD::Vector pos);
	virtual void SetPos(SGD::Point pos);

	virtual void SetOffset(SGD::Vector offset);
	virtual void SetOffset(SGD::Point offset);

	Projection Project(Axis axis);

private:
	std::vector<SGD::Vector> m_vPoints;
	std::vector<Axis> m_vAxes;
};