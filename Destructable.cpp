/***************************************************************
|	File:		Destructable.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A destructable class that the player can
|				destroy for points
***************************************************************/

#include "Destructable.h"

#include "Image.h"

Destructable::Destructable(SGD::HTexture standing, int swidth, int sheight, SGD::HTexture hit, int hwidth, int hheight)
{
	m_pStanding = new Image(standing, swidth, sheight);
	m_pHit = new Image(hit, hwidth, hheight);
}

Destructable::~Destructable()
{
}

void Destructable::Update()
{
	if (true) // Been hit with a bullet
	{
		m_bHit = true;
	}

	// Figure out driving over stuffs
}