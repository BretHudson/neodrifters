/***************************************************************
|	File:		Bullet.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A class that acts as a projectile
***************************************************************/

#include "Bullet.h"
#include "Graphic.h"
#include "Image.h"

#include "Vehicle.h"
#include "Player.h"

#include "Util.h"

#include "Game.h"

#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "DestroyEntityMessage.h"
#include "DamageMessage.h"

#include "GunManager.h"

#include "PolygonCollider.h"

#include "Tags.h"

#include "Text.h"

#include <sstream>

Bullet::Bullet()
{
	
}

Bullet::Bullet(const Bullet& other) : Entity(other)
{
	m_fDamage = other.m_fDamage;
	m_bExplosive = other.m_bExplosive;

	m_fSpeed = other.m_fSpeed;

	m_iLife = other.m_iLife;

	m_bHoming = other.m_bHoming;

	PolygonCollider* poly = new PolygonCollider();

	std::vector<SGD::Vector> points;
	points.push_back(SGD::Vector(-6, -6));
	points.push_back(SGD::Vector(6, -6));
	points.push_back(SGD::Vector(6, 6));
	points.push_back(SGD::Vector(-6, 6));
	poly->SetPoints(points);
	poly->AddTag((int)Tags::BULLET);

	SetCollider(poly);

	//volumeText = new Text("100", 0, 0);
	//volumeText->SetScale(0.5f);
	//AddGraphic(volumeText);

	m_hHit = SGD::AudioManager::GetInstance()->LoadAudio("resource/audio/be_car_hurt.wav");
}

Bullet::~Bullet()
{
	if (m_pTarget)
		m_pTarget->Release();
	if (m_pOwner)
		m_pOwner->Release();

	SGD::AudioManager::GetInstance()->UnloadAudio(m_hHit);
}

void Bullet::Update()
{
	if (m_bHoming)
	{
		// Find a target if none is selected
		if (!m_pTarget)
		{
			//
		}

		// Turn to face the correct way if homing
		if (m_pTarget)
		{
			SGD::Vector toTarget = m_pTarget->GetCenter() - m_ptPosition;

			SGD::Vector orientation = SGD::Vector(1, 0);
			orientation.Rotate(m_fRotation);
			float angle = orientation.ComputeAngle(toTarget);

			if (orientation.ComputeSteering(toTarget) < 0)
				angle = -angle;

			m_fRotation += Util::Clamp(angle, -SGD::PI / 180 * 3, SGD::PI / 180 * 3);
		}
	}

	SetRotation(m_fRotation);

	// Change speed
	SGD::Vector newVelocity = SGD::Vector(1, 0);
	newVelocity *= m_fSpeed;
	newVelocity.Rotate(m_fRotation);
	SetVelocity(newVelocity);

	// Check collisions
	int* tags = new int[3];
	tags[0] = (int)Tags::SOLID;
	tags[1] = (int)Tags::VEHICLE;
	tags[2] = -1;

	/*std::ostringstream oss;
	oss << m_iVolume;
	volumeText->SetText(oss.str().c_str());*/

	for (int i = 0; i < 2; ++i)
	{
		//SGD::Vector v((float)i, 0.0f);
		SGD::Vector v((float)i * m_fSpeed * 0.33f, 0.0f);
		v.Rotate(m_fRotation);
		//if (Collide(v.x, v.y, tags))
		Collider* collider = Collide(v.x, v.y, tags);
		if ((collider) && (collider->GetEntity() != m_pOwner))
		{
			SGD::AudioManager::GetInstance()->SetAudioVolume(m_hHit, m_iVolume);
			SGD::AudioManager::GetInstance()->PlayAudio(m_hHit);
			SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(this));
			SGD::MessageManager::GetInstance()->QueueMessage(new DamageMessage(collider->GetEntity(), int(m_fDamage)));
		}
	}

	//tags[0] = (int)Tags::VEHICLE;

	//if (Collide(-20.0f, 0.0f, tags) != nullptr)
	//	m_fSpeed = 10.0f;
	//else
	//	m_fSpeed = 0.0f;

	delete[] tags;

	// Move that thang!
	Entity::Update();

	// Life stuff
	--m_iLife;
	if (m_iLife <= 0)
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(this));
}

void Bullet::Render()
{
	for (std::vector<Graphic*>::iterator iter = m_vGraphics.begin(); iter != m_vGraphics.end(); ++iter)
		(*iter)->SetRotation(m_fRotation);
	Entity::Render();

	if ((m_bHoming) && (m_pTarget) && (dynamic_cast<Player*>(m_pTarget)))
	{
		SGD::Vector toTarget = m_pTarget->GetCenter() - m_ptPosition;

		SGD::Vector orientation = SGD::Vector(1, 0);
		//orientation.Rotate(m_fRotation);
		float angle = orientation.ComputeAngle(toTarget);

		if (orientation.ComputeSteering(toTarget) < 0)
			angle = -angle;

		GunManager::GetIncomingImage()->SetRotation(angle);

		SGD::Point drawPoint = m_pTarget->GetCenter() - Game::GetInstance()->GetCamera();

		SGD::Vector drawOffset = SGD::Vector(50, 0);
		drawOffset.Rotate(angle);
		drawPoint -= drawOffset;

		GunManager::GetIncomingImage()->Render(drawPoint);
	}
}

void Bullet::SetRotation(float angle)
{
	Entity::SetRotation(angle);
}

void Bullet::SetTarget(Vehicle* target)
{
	if (m_pTarget)
		m_pTarget->Release();
	m_pTarget = target;
	m_pTarget->AddRef();
}

void Bullet::SetOwner(Entity* owner)
{
	m_pOwner = owner;
	m_pOwner->AddRef();
}

void Bullet::Reposition(float width, float height)
{
	float halfWidth = width * 0.5f;
	float halfHeight = height * 0.5f;

	if (m_pTarget)
	{
		if (GetPosition().x < m_pTarget->GetPosition().x - halfWidth)
			SetPosition(GetPosition() + SGD::Vector(width, 0.0f));
		if (GetPosition().x > m_pTarget->GetPosition().x + halfWidth)
			SetPosition(GetPosition() - SGD::Vector(width, 0.0f));

		if (GetPosition().y < m_pTarget->GetPosition().y - halfHeight)
			SetPosition(GetPosition() + SGD::Vector(0.0f, height));
		if (GetPosition().y > m_pTarget->GetPosition().y + halfHeight)
			SetPosition(GetPosition() - SGD::Vector(0.0f, height));
	}
}