/***************************************************************
|	File:		MenuCircleOne.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle One
***************************************************************/

#include "MenuCircleOne.h"
#include "Game.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"

#include "GunManager.h"
#include "Gun.h"
#include "VehicleManager.h"
#include "Vehicle.h"

#include "Text.h"
#include "Image.h"

#include "Util.h"

#include "Input.h"

#include <sstream>

MenuCircleOne::MenuCircleOne(SGD::HTexture bright, SGD::HTexture dim, int* vehicleID, int* leftGunID, int* rightGunID) : MenuCircle(bright, dim)
{
	m_hLeftCircleBright = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/small_circle 1/be_small_circle1_bright.png");
	m_hLeftCircleDim = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/small_circle 1/be_small_circle1_dim.png");

	m_hRightCircleBright = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/small_circle 2/be_small_circle2_bright.png");
	m_hRightCircleDim = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/small_circle 2/be_small_circle2_dim.png");

	m_pLeftCircleBright = new Image(m_hLeftCircleBright, 298, 298);
	m_pLeftCircleBright->SetOrigin(500, -170);
	m_pLeftCircleDim = new Image(m_hLeftCircleDim, 298, 298);
	m_pLeftCircleDim->SetOrigin(500, -170);
	m_pRightCircleBright = new Image(m_hRightCircleBright, 298, 298);
	m_pRightCircleBright->SetOrigin(-202, -170);
	m_pRightCircleDim = new Image(m_hRightCircleDim, 298, 298);
	m_pRightCircleDim->SetOrigin(-202, -170);

	AddGraphic(m_pLeftCircleBright);
	AddGraphic(m_pLeftCircleDim);
	AddGraphic(m_pRightCircleBright);
	AddGraphic(m_pRightCircleDim);

	m_pTexts.push_back(new Text("Vehicle", 350, 320));
	m_pTexts.push_back(new Text("Gun Left", 20, 300));
	m_pTexts.push_back(new Text("Gun Right", 20, 410));

	m_pTexts[1]->SetScale(0.5f);
	m_pTexts[2]->SetScale(0.5f);

	m_pVehicleID = vehicleID;
	m_pLeftGunID = leftGunID;
	m_pRightGunID = rightGunID;

	m_hArrow = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/playmenu/be_arrow.png");

	m_pArrowLeft = new Image(m_hArrow, 21, 32);
	m_pArrowLeft->SetScaleX(-1.0f);
	m_pArrowLeft->CenterOrigin();
	m_pArrowRight = new Image(m_hArrow, 21, 32);
	m_pArrowRight->CenterOrigin();

	m_ptVehicleArrowLeft = SGD::Point(420, 252);
	m_ptVehicleArrowRight = SGD::Point(540, 252);
	m_rtVehicleArrowLeft = SGD::Rectangle(420 - 16, 252 - 22, 420 + 16, 252 + 22);
	m_rtVehicleArrowRight = SGD::Rectangle(540 - 16, 252 - 22, 540 + 16, 252 + 22);

	m_ptGunLeftArrowLeft = SGD::Point(100, 270);
	m_ptGunLeftArrowRight = SGD::Point(180, 270);
	m_rtGunLeftArrowLeft = SGD::Rectangle(100 - 16, 270 - 22, 100 + 16, 270 + 22);
	m_rtGunLeftArrowRight = SGD::Rectangle(180 - 16, 270 - 22, 180 + 16, 270 + 22);

	m_ptGunRightArrowLeft = SGD::Point(100, 380);
	m_ptGunRightArrowRight = SGD::Point(180, 380);
	m_rtGunRightArrowLeft = SGD::Rectangle(100 - 16, 380 - 22, 100 + 16, 380 + 22);
	m_rtGunRightArrowRight = SGD::Rectangle(180 - 16, 380 - 22, 180 + 16, 380 + 22);
}

MenuCircleOne::~MenuCircleOne()
{
	for (std::vector<Text*>::iterator iter = m_pTexts.begin(); iter != m_pTexts.end(); ++iter)
		delete *iter;
	m_pTexts.clear();

	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hArrow);
	delete m_pArrowLeft;
	m_pArrowLeft = nullptr;
	delete m_pArrowRight;
	m_pArrowRight = nullptr;

	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hLeftCircleBright);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hLeftCircleDim);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hRightCircleBright);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hRightCircleDim);
}

void MenuCircleOne::Update()
{
	MenuCircle::Update();

	std::ostringstream oss;
	oss << Game::GetVehicleManager()->GetVehicle(*m_pVehicleID)->GetName();
	m_pTexts[0]->SetText(oss.str().c_str());
	m_pTexts[0]->SetOriginX(-130 + ((float)oss.str().size() * 32.0f / 2.0f));
	
	oss.str("");
	oss << Game::GetGunManager()->GetGun(*m_pLeftGunID)->GetName();
	m_pTexts[1]->SetText(oss.str().c_str());
	m_pTexts[1]->SetOriginX(-120 + ((float)oss.str().size() * 16.0f / 2.0f));

	oss.str("");
	oss << Game::GetGunManager()->GetGun(*m_pRightGunID)->GetName();
	m_pTexts[2]->SetText(oss.str().c_str());
	m_pTexts[2]->SetOriginX(-120 + ((float)oss.str().size() * 16.0f / 2.0f));

	m_fCarRotation = Util::CircleClamp(m_fCarRotation + SGD::PI * 0.005f, 0.0f, SGD::PI * 2);

	if (Input::GetMousePressed(SGD::Key::MouseLeft))
	{
		SGD::Rectangle mousePos = SGD::Rectangle(Input::GetMouseXY() - SGD::Vector(1, 1), Input::GetMouseXY() + SGD::Vector(1, 1));

		// Vehicle
		if (m_rtVehicleArrowLeft.IsIntersecting(mousePos))
			--(*m_pVehicleID);
		if (m_rtVehicleArrowRight.IsIntersecting(mousePos))
			++(*m_pVehicleID);

		// Gun Left
		if (m_rtGunLeftArrowLeft.IsIntersecting(mousePos))
			--(*m_pLeftGunID);
		if (m_rtGunLeftArrowRight.IsIntersecting(mousePos))
			++(*m_pLeftGunID);

		// Gun Right
		if (m_rtGunRightArrowLeft.IsIntersecting(mousePos))
			--(*m_pRightGunID);
		if (m_rtGunRightArrowRight.IsIntersecting(mousePos))
			++(*m_pRightGunID);
	}
}

void MenuCircleOne::Render()
{
	MenuCircle::Render();

	if (!m_bDisplayOn)
	{
		m_pLeftCircleDim->SetAlpha(255);
		m_pRightCircleDim->SetAlpha(255);
		m_pLeftCircleBright->SetAlpha(0);
		m_pRightCircleBright->SetAlpha(0);
	}
	else
	{
		m_pLeftCircleDim->SetAlpha(0);
		m_pRightCircleDim->SetAlpha(0);
		m_pLeftCircleBright->SetAlpha(255);
		m_pRightCircleBright->SetAlpha(255);
	}\

	if ((m_fRotation == 0) && (m_bDisplayOn) && (m_iFlickers == 0) && (m_iFlickerTimer <= 0))
	{
		RenderText();
		RenderVehicle();
		RenderGuns();
		RenderArrows();
	}

	// Render cursor
	/*SGD::Rectangle mousePos = SGD::Rectangle(Input::GetMouseXY() - SGD::Vector(1, 1), Input::GetMouseXY() + SGD::Vector(1, 1));

	SGD::GraphicsManager::GetInstance()->DrawRectangle(mousePos, SGD::Color(255, 255, 255));*/
}

void MenuCircleOne::RenderText()
{
	for (std::vector<Text*>::iterator iter = m_pTexts.begin(); iter != m_pTexts.end(); ++iter)
		(*iter)->Render(SGD::Point(0, 0));
}

void MenuCircleOne::RenderVehicle()
{
	Graphic* graphic = Game::GetInstance()->GetVehicleManager()->GetVehicle(*m_pVehicleID)->GetImage();
	Image* image = dynamic_cast<Image*>(graphic);
	SGD::Vector originalOrigin = image->GetOrigin();
	image->SetRotation(m_fCarRotation);
	image->CenterOrigin();
	image->Render(SGD::Point(480, 250));
	image->SetOrigin(originalOrigin);
}

void MenuCircleOne::RenderGuns()
{
	Graphic* graphic = Game::GetInstance()->GetGunManager()->GetGun(*m_pLeftGunID)->GetImage();
	Image* image = dynamic_cast<Image*>(graphic);
	SGD::Vector originalOrigin = image->GetOrigin();
	image->SetRotation(0.6f);
	image->CenterOrigin();
	image->Render(SGD::Point(140, 270));
	image->SetOrigin(originalOrigin);

	graphic = Game::GetInstance()->GetGunManager()->GetGun(*m_pRightGunID)->GetImage();
	image = dynamic_cast<Image*>(graphic);
	originalOrigin = image->GetOrigin();
	image->SetRotation(0.6f);
	image->CenterOrigin();
	image->Render(SGD::Point(140, 380));
	image->SetOrigin(originalOrigin);
}

void MenuCircleOne::RenderArrows()
{
	// Collisions
	if (false)
	{
		SGD::GraphicsManager::GetInstance()->DrawRectangle(m_rtVehicleArrowLeft, SGD::Color(255, 255, 255));
		SGD::GraphicsManager::GetInstance()->DrawRectangle(m_rtVehicleArrowRight, SGD::Color(255, 255, 255));

		SGD::GraphicsManager::GetInstance()->DrawRectangle(m_rtGunLeftArrowLeft, SGD::Color(255, 255, 255));
		SGD::GraphicsManager::GetInstance()->DrawRectangle(m_rtGunLeftArrowRight, SGD::Color(255, 255, 255));

		SGD::GraphicsManager::GetInstance()->DrawRectangle(m_rtGunRightArrowLeft, SGD::Color(255, 255, 255));
		SGD::GraphicsManager::GetInstance()->DrawRectangle(m_rtGunRightArrowRight, SGD::Color(255, 255, 255));
	}

	// Vehicle arrows
	m_pArrowLeft->Render(m_ptVehicleArrowLeft);
	m_pArrowRight->Render(m_ptVehicleArrowRight);

	// Gun left arrows
	m_pArrowLeft->Render(m_ptGunLeftArrowLeft);
	m_pArrowRight->Render(m_ptGunLeftArrowRight);

	// Gun right arrows
	m_pArrowLeft->Render(m_ptGunRightArrowLeft);
	m_pArrowRight->Render(m_ptGunRightArrowRight);
}