/***************************************************************
|	File:		Guns.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A container class that holds a Vehicle's Gun objects
***************************************************************/

#pragma once

#include "../SGD Wrappers/SGD_Geometry.h"

class Gun;
class Vehicle;

class Guns
{
public:
	Guns(Gun* _left, Gun* _right);
	~Guns();

	//void Update();
	
	void PositionGuns(SGD::Point ptLeft, SGD::Point ptRight, float angle);
	void Shoot(bool pressed, bool down);

	Gun* GetLeft() const { return m_pLeft; }
	Gun* GetRight() const { return m_pRight; }

	// TODO: Add a reference
	void SetOwner(Vehicle* owner) { m_pOwner = owner; }

private:
	Gun* m_pLeft;
	Gun* m_pRight;

	Vehicle* m_pOwner;
};

