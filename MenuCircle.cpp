/***************************************************************
|	File:		MenuCircle.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle
***************************************************************/

#include "MenuCircle.h"

#include "Image.h"

#include "Util.h"

MenuCircle::MenuCircle(SGD::HTexture bright, SGD::HTexture dim)
{
	m_pBright = new Image(bright, SGD::Size(398, 398));
	m_pBright->SetOrigin(SGD::Vector(398 / 2, -54));
	AddGraphic(m_pBright);

	m_pDim = new Image(dim, SGD::Size(398, 398));
	m_pDim->SetOrigin(SGD::Vector(398 / 2, -54));
	AddGraphic(m_pDim);

	SetPosition(SGD::Point(480, 32));

	m_iFlickers = Util::RandomRange(3, 5);
}

MenuCircle::~MenuCircle()
{
	//
}

void MenuCircle::Update()
{
	for (std::vector<Graphic*>::iterator iter = m_vGraphics.begin(); iter != m_vGraphics.end(); ++iter)
		(*iter)->SetRotation(GetRotation());

	if ((!m_bLocked) && (m_iFlickers == 0))
		ResetFlicker();

	m_bDisplayOn = false;

	if (GetRotation() == 0)
	{
		--m_iFlickerTimer;
		if (m_iFlickerTimer <= 0)
		{
			m_bDisplayOn = true;
			if (m_iFlickers > 0)
			{
				ResetFlickerTimer();
				--m_iFlickers;
			}
		}
	}

	//m_bDisplayOn = GetRotation() == 0;

	int gfx1On = int(m_bDisplayOn) * 255;
	int gfx2On = int(!m_bDisplayOn) * 255;

	m_vGraphics[0]->SetAlpha(gfx1On);
	m_vGraphics[1]->SetAlpha(gfx2On);
}

void MenuCircle::Render()
{
	Entity::Render();
}

void MenuCircle::ResetFlicker()
{
	m_iFlickers = Util::RandomRange(3, 5);
	ResetFlickerTimer();
}

void MenuCircle::ResetFlickerTimer()
{
	m_iFlickerTimer = Util::RandomRange(4, 11);
}