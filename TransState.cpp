#include "TransState.h"
#include "Game.h"
#include "Image.h"
#include "Entity.h"
#include "EntityManager.h"
#include "../SGD Wrappers/SGD_Geometry.h"

TransState::TransState()
{
	
}

TransState::~TransState()
{
}

void TransState::Enter()
{
	m_hTransition = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/be_transition.png");
	m_pTransitionImage = new Image(m_hTransition, 2048, 544);
	m_pTransitionEntity = new Entity();
	m_pTransitionEntity->SetLayer(-100000);
	m_pTransitionEntity->SetPosition(SGD::Point(-513.0f, 0.0f));
	m_pTransitionEntity->AddGraphic(m_pTransitionImage);
	m_pEntities = new EntityManager();
	//m_pEntities->AddEntity(m_pTransitionEntity, 0);
}

void TransState::Exit()
{
	m_pEntities->RemoveAll();
	delete m_pEntities;
	m_pEntities = nullptr;

	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hTransition);
	m_pTransitionEntity->Release();
}

void TransState::Update()
{
	SGD::Point transPos = m_pTransitionEntity->GetPosition();

	if ((m_pToState != nullptr) && (transPos.x <= -513))
	{
		Game::GetInstance()->ChangeState(m_pToState);
		m_pToState = nullptr;
	}
	else if (transPos.x > -2048)
	{
		m_pTransitionEntity->SetPosition(transPos + SGD::Vector(-30, 0));
		SGD::Vector camera = Game::GetInstance()->GetCamera();
		m_pTransitionImage->SetOffset(camera);
	}
	else
		m_pTransitionImage->SetAlpha(0);
}

void TransState::Render()
{
	// Render the entities
	m_pEntities->RenderAll();
	m_pTransitionEntity->Render();
}

void TransState::FadeToState(IGameState* toState)
{
	m_pToState = toState;

	m_pTransitionEntity->SetPosition(SGD::Point(1300.0f, 0));

	m_pTransitionImage->SetAlpha(255);
}