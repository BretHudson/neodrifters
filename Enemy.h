/***************************************************************
|	File:		Enemy.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A Vehicle that is controlled by the computer
***************************************************************/

#pragma once
#include "Vehicle.h"
class Enemy :
	public Vehicle
{
public:
	Enemy();
	Enemy(const Enemy& other);
	Enemy(const Vehicle& other);
	~Enemy();

	int	GetType(void) const override { return ENT_ENEMY; }

	void Update();
	void Shoot(bool pressed, bool down);
};

