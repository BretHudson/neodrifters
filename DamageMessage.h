/***************************************************************
|	File:		DamageMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Damage Message
***************************************************************/

#pragma once

#include "..\SGD Wrappers\SGD_Message.h"

class Entity;

class DamageMessage :
	public SGD::Message
{
public:
	DamageMessage(Entity* entity, int damage);
	~DamageMessage();

	Entity* GetEntity(void) const { return m_pEntity; }
	int GetDamage(void) const { return m_iDamage; }

private:
	Entity* m_pEntity;
	int m_iDamage;
};

