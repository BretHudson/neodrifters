/***************************************************************
|	File:		CreateEnemyMessage.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Create Enemy Message
***************************************************************/

#pragma once

#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Message.h"

class CreateEnemyMessage :
	public SGD::Message
{
public:
	CreateEnemyMessage(SGD::Point point);
	~CreateEnemyMessage();

	SGD::Point GetSpawnPoint() const { return m_ptSpawnPoint; }

private:
	SGD::Point m_ptSpawnPoint;
};

