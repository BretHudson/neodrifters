/***************************************************************
|	File:		CreateTankMessage.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Create Tank Message
***************************************************************/

#pragma once

#include "..\SGD Wrappers\SGD_Message.h"

class CreateTankMessage :
	public SGD::Message
{
public:
	CreateTankMessage();
	~CreateTankMessage();
};

