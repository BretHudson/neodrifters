/***************************************************************
|	File:		Player.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A Vehicle that is controlled by the player
***************************************************************/

#pragma once
#include "Vehicle.h"

class Spritemap;

class Player :
	public Vehicle
{
public:
	Player();
	Player(const Player& other);
	Player(const Vehicle& other);
	~Player();
	
	int	GetType(void) const override { return ENT_PLAYER; }

	void Update();
	void Screenshake(float shake);
	void Shoot(bool pressed, bool down);

private:
	SGD::HAudio m_hDriving = SGD::INVALID_HANDLE;
};

