/***************************************************************
|	File:		MenuCircle.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle
***************************************************************/

#pragma once

#include "Entity.h"

class Image;

class MenuCircle :
	public Entity
{
public:
	MenuCircle(SGD::HTexture bright, SGD::HTexture dim);
	virtual ~MenuCircle();

	void Update();
	void Render();

	void SetLocked(bool locked) { m_bLocked = locked; }

	void ResetFlicker();
	void ResetFlickerTimer();

protected:
	Image* m_pBright;
	Image* m_pDim;

	int m_iFlickers = 0;
	int m_iFlickerTimer = 0;
	bool m_bDisplayOn = false;
	bool m_bLocked = true;
};

