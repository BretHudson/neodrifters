/***************************************************************
|	File:		GameplayState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	GameplayState class initializes & runs the game logic
***************************************************************/

#include "GameplayState.h"

#include "Game.h"
#include "MainMenuState.h"

#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_String.h"

#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_Message.h"
#include "MessageID.h"

#include "CreateBulletMessage.h"
#include "CreateEnemyMessage.h"
#include "CreateTankMessage.h"
#include "DamageMessage.h"
#include "DestroyEntityMessage.h"
#include "PortalDestroyedMessage.h"
#include "ScoreMessage.h"

#include "Entity.h"
#include "EntityManager.h"

#include "Bullet.h"

#include "GunManager.h"
#include "VehicleManager.h"

#include "Vehicle.h"
#include "Player.h"
#include "Enemy.h"
#include "Tank.h"
#include "Guns.h"
#include "Gun.h"
#include "Image.h"
#include "Text.h"
#include "Input.h"

#include "Collider.h"
#include "Tags.h"

#include "Util.h"

#include "EnemySpawner.h"

#include <cstdlib>
#include <cassert>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <math.h>


/**************************************************************/
// GetInstance
//	- allocate static global instance
//	- return THE instance
/*static*/ GameplayState* GameplayState::GetInstance(void)
{
	static GameplayState s_Instance;	// stored in global memory once
	return &s_Instance;
}


/**************************************************************/
// Enter
//	- reset game
//	- load resources
//	- set up entities
/*virtual*/ void GameplayState::Enter(void)
{
	TransState::Enter();

	m_fTime = 36000;

	// Set background color
	SGD::GraphicsManager::GetInstance()->SetClearColor({ 0, 0, 0 });	// black

	// Initialize the Event & Message Managers
	SGD::EventManager::GetInstance()->Initialize();
	SGD::MessageManager::GetInstance()->Initialize(&MessageProc);

	// Uncomment this to spawn a Tank at the beginning of the game, for testing purposes
	//SGD::MessageManager::GetInstance()->QueueMessage(new CreateTankMessage());

	m_hBackgroundMusic = SGD::AudioManager::GetInstance()->LoadAudio("resource/audio/be_fly.xwm");

	//SGD::AudioManager::GetInstance()->SetAudioVolume(m_hBackgroundMusic, 0);

	SGD::AudioManager::GetInstance()->PlayAudio(m_hBackgroundMusic, true);

	m_hBackground = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/be_level.png");
	m_hSpawner = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/be_portal.png");

	m_hTankBody = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/vehicles/tank/be_body.png");
	m_hTankTurrets = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/vehicles/tank/be_turrets.png");
	m_hTankMainGun = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/vehicles/tank/be_gun.png");

	m_pPlayer = CreatePlayer(m_iVehicleID, m_iLeftGunID, m_iRightGunID);

	m_bPaused = false;
	m_iPausedSelect = 0;

	m_iScore = 0;
	m_iPortals = 4;

	AddSpawners();

	m_pScoreText = new Text("Score: ", 20, 20);
	m_pScoreText->SetScale(0.5f);
	m_pPortalText = new Text("Portals Left: ", 700, 20);
	m_pPortalText->SetScale(0.5f);

	m_pPausedText = new Text("PAUSED", 380, 50);
	m_pPausedText1 = new Text("RESUME", 380, 150);
	m_pPausedText2 = new Text("QUIT", 380, 200);

	m_pTimerText = new Text("Time Left: ", 300, 20);
	m_pTimerText->SetScale(0.5f);

	/*Circle* circle = new Circle();
	circle->SetRenderImage(new Image(m_hCar3));
	m_pEntities->AddEntity(m_pCircle = circle, 0);
	m_pCircle->SetPosition(SGD::Point(50, 50));
	m_pCircle->SetSize(SGD::Size(36, 64));*/

	Game::GetInstance()->SetCamera(m_pPlayer->GetPosition());

	m_hHit = SGD::AudioManager::GetInstance()->LoadAudio("resource/audio/be_car_hurt.wav");
}


/**************************************************************/
// Exit
//	- deallocate entities
//	- unload resources
/*virtual*/ void GameplayState::Exit(void)
{
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hBackground);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hSpawner);

	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hTankBody);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hTankTurrets);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hTankMainGun);

	SGD::AudioManager::GetInstance()->UnloadAudio(m_hBackgroundMusic);

	SGD::AudioManager::GetInstance()->UnloadAudio(m_hHit);

	m_pPlayer->Release();

	delete m_pScoreText;
	m_pScoreText = nullptr;

	delete m_pPortalText;
	m_pPortalText = nullptr;

	delete m_pPausedText;
	m_pPausedText = nullptr;

	delete m_pPausedText1;
	m_pPausedText1 = nullptr;

	delete m_pPausedText2;
	m_pPausedText2 = nullptr;

	delete m_pTimerText;
	m_pTimerText = nullptr;


	// Terminate & deallocate the SGD wrappers
	SGD::EventManager::GetInstance()->Terminate();
	SGD::EventManager::DeleteInstance();

	SGD::MessageManager::GetInstance()->Terminate();
	SGD::MessageManager::DeleteInstance();

	TransState::Exit();
}


/**************************************************************/
// Input
//	- handle user input
/*virtual*/ bool GameplayState::Input(void)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	if (Input::CheckPressed("pause"))
	{
		FadeToState(MainMenuState::GetInstance());
		/*m_bPaused = !m_bPaused;
		m_iPausedSelect = 0;*/
	}

	if (m_bPaused)
	{
		if (Input::CheckPressed("menuup"))
			--m_iPausedSelect;
		if (Input::CheckPressed("menudown"))
			++m_iPausedSelect;

		m_iPausedSelect = Util::CircleClamp(m_iPausedSelect, 0, 1);

		if (Input::CheckPressed("menuselect"))
		{
			if (m_iPausedSelect == 0)
				m_bPaused = false;
			else if (m_iPausedSelect == 1)
				FadeToState(MainMenuState::GetInstance());
		}
	}

	return true;	// keep playing
}


SGD::Vector lerp(SGD::Vector a, SGD::Vector b, float amount)
{
	SGD::Vector result;
	result.x = amount * (b.x - a.x) + a.x;
	result.y = amount * (b.y - a.y) + a.y;
	return result;
}

/**************************************************************/
// Update
//	- update game entities
/*virtual*/ void GameplayState::Update(void)
{
	if (!m_bPaused)
	{
		--m_fTime;

		if (m_fTime <= 0)
			Game::GetInstance()->Lose();

		// Update the entities
		m_pEntities->UpdateAll();

		FixEntityPlacement();

		SGD::Point cameraPos = { Game::GetInstance()->GetCamera().x, Game::GetInstance()->GetCamera().y };

		SGD::Vector cameraOffset;
		Vehicle* player = static_cast<Vehicle*>(m_pPlayer);

		cameraOffset.x = cameraPos.x + ((player->GetPosition().x - 960 / 2 + cos(player->GetRotation()) * (player->GetSpeed() + 6) * 20) - cameraPos.x) * 0.19f;
		cameraOffset.y = cameraPos.y + ((player->GetPosition().y - 540 / 2 + sin(player->GetRotation()) * (player->GetSpeed() + 6) * 10) - cameraPos.y) * 0.19f;

		cameraOffset.x = floor(cameraOffset.x);
		cameraOffset.y = floor(cameraOffset.y);

		Game::GetInstance()->SetCamera(cameraOffset);

		// Process the events & messages
		SGD::EventManager::GetInstance()->Update();
		SGD::MessageManager::GetInstance()->Update();

		// Update the score

		std::ostringstream oss;
		oss << "Score: " << m_iScore;
		m_pScoreText->SetText(oss.str().c_str());

		oss.str("");
		oss << "Portals Left: " << m_iPortals;
		m_pPortalText->SetText(oss.str().c_str());

		oss.str("");
		oss << "Time Left: " << int(m_fTime / 60) << " s";
		m_pTimerText->SetText(oss.str().c_str());
	}

	TransState::Update();
}


/**************************************************************/
// Render
//	- render the game entities
/*virtual*/ void GameplayState::Render(void)
{
	// Render the background
	SGD::Point drawPoint = { Game::GetInstance()->GetCamera().x, Game::GetInstance()->GetCamera().y };

	// Offsets for the extra backgrounds
	SGD::Vector goUp = { 0, -4800 };
	SGD::Vector goLeft = { -8000, 0 };
	SGD::Vector goDown = { 0, 4800 };
	SGD::Vector goRight = { 8000, 0 };

	// Left
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goUp + goLeft);
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goLeft);
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goLeft + goDown);

	// Middle
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goUp);
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint);
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goDown);

	// Right
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goUp + goRight);
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goRight);
	SGD::GraphicsManager::GetInstance()->DrawTexture(m_hBackground, -drawPoint + goRight + goDown);

	TransState::Render();

	// Render the score
	m_pScoreText->Render(SGD::Point(0, 0));

	m_pTimerText->Render(SGD::Point(0, 0));

	// Render how many portals are left
	m_pPortalText->Render(SGD::Point(0, 0));

	if (m_bPaused)
	{
		m_pPausedText->Render(SGD::Point(0, 0));
		m_pPausedText1->Render(SGD::Point((m_iPausedSelect == 0) ? 50.0f : 0.0f, 0.0f));
		m_pPausedText2->Render(SGD::Point((m_iPausedSelect == 1) ? 50.0f : 0.0f, 0.0f));
	}
}


/**************************************************************/
// MessageProc
//	- process messages queued in the MessageManager
//	- STATIC METHOD
//		- does NOT have invoking object!!!
//		- must use singleton to access members
/*static*/ void GameplayState::MessageProc(const SGD::Message* pMsg)
{
	/* Show warning when a Message ID enumerator is not handled */
#pragma warning( push )
#pragma warning( 1 : 4061 )

	// What type of message?
	switch (pMsg->GetMessageID())
	{
		case MessageID::MSG_CREATE_BULLET: {
			const CreateBulletMessage* bulletMessage = dynamic_cast<const CreateBulletMessage*>(pMsg);

			unsigned int numBullets = Game::GetInstance()->GetGunManager()->GetNumBullets(bulletMessage->GetGunID());
			Bullet* bulletToClone = Game::GetInstance()->GetGunManager()->GetBullet(bulletMessage->GetVehicle()->GetID() % numBullets, bulletMessage->GetGunID());

			Bullet* bullet = new Bullet(*bulletToClone);
			bullet->SetPosition(bulletMessage->GetSpawnPoint());
			bullet->SetRotation(bulletMessage->GetVehicle()->GetRotation());
			//bullet->SetTarget(bulletMessage->GetVehicle());
			bullet->SetOwner(bulletMessage->GetVehicle());

			GameplayState::GetInstance()->m_pEntities->AddEntity(bullet, 0);

			bullet->Release();
		} break;
		case MessageID::MSG_CREATE_ENEMY: {
			const CreateEnemyMessage* enemyMessage = dynamic_cast<const CreateEnemyMessage*>(pMsg);

			// It feels fair to have the enemy always have two of the same gun, so I have made it so the id for the left and right gun is derived from the same value
			int vehicleID = Util::RandomRange(0, Game::GetInstance()->GetVehicleManager()->GetNumVehicles() - 1);
			int gunID = Util::RandomRange(0, Game::GetInstance()->GetGunManager()->GetNumGuns() - 1);

			Entity* enemy = GameplayState::GetInstance()->CreateEnemy(vehicleID, gunID, gunID, 0);

			enemy->SetPosition(enemyMessage->GetSpawnPoint());
			dynamic_cast<Vehicle*>(enemy)->PositionGuns();

		} break;
		case MessageID::MSG_SCORE: {
			const ScoreMessage* scoreMessage = dynamic_cast<const ScoreMessage*>(pMsg);

			GetInstance()->m_iScore += scoreMessage->GetScore();
		} break;
		case MessageID::MSG_PORTAL_DESTROYED: {
			const PortalDestroyedMessage* portalMessage = dynamic_cast<const PortalDestroyedMessage*>(pMsg);

			--GetInstance()->m_iPortals;

			if (GetInstance()->m_iPortals == 0)
				SGD::MessageManager::GetInstance()->QueueMessage(new CreateTankMessage());
		} break;
		case MessageID::MSG_DESTROY_ENTITY: {
			const DestroyEntityMessage* destroyMessage = dynamic_cast<const DestroyEntityMessage*>(pMsg);

			Entity* entity = destroyMessage->GetEntity();

			GetInstance()->m_pEntities->RemoveEntity(entity);
		} break;
		case MessageID::MSG_DAMAGE: {
			const DamageMessage* damageMessage = dynamic_cast<const DamageMessage*>(pMsg);

			Entity* entity = damageMessage->GetEntity();

			entity->ApplyDamage(damageMessage->GetDamage());

		} break;
		case MessageID::MSG_CREATE_TANK: {

			const CreateTankMessage* createMessage = dynamic_cast<const CreateTankMessage*>(pMsg);

			GetInstance()->CreateTank();

		} break;
		case MessageID::MSG_UNKNOWN:
		default: {
			OutputDebugStringW(L"Game::MessageProc - unknown message id\n");
		} break;
	}

	/* Restore previous warning levels */
#pragma warning( pop )

}

void GameplayState::SetPlayerVars(int veh, int lgun, int rgun)
{
	m_iVehicleID = veh;
	m_iLeftGunID = lgun;
	m_iRightGunID = rgun;
}

Entity* GameplayState::CreatePlayer(int vehicleID, int gunLeftID, int gunRightID)
{
	Vehicle* player = new Player(*Game::GetInstance()->GetVehicleManager()->GetVehicle(vehicleID));
	player->SetPosition(SGD::Point(1250, 1030));

	// Initialize the left Gun
	Gun* gunLeft = new Gun(*Game::GetInstance()->GetGunManager()->GetGun(gunLeftID));
	gunLeft->SetOwner(player);
	gunLeft->SetSide("left");

	// Initialize the right Gun
	Gun* gunRight = new Gun(*Game::GetInstance()->GetGunManager()->GetGun(gunRightID));
	gunRight->SetOwner(player);
	gunRight->SetSide("right");

	// Set the player's Guns
	player->SetGuns(new Guns(gunLeft, gunRight));

	// Add entities to the EntityManager
	m_pEntities->AddEntity(player, 0);
	m_pEntities->AddEntity(gunLeft, 0);
	m_pEntities->AddEntity(gunRight, 0);

	// Release those guns
	gunLeft->Release();
	gunRight->Release();

	return player;
}

Entity* GameplayState::CreateEnemy(int vehicleID, int gunLeftID, int gunRightID, int behavior)
{
	Vehicle* enemy = new Enemy(*Game::GetInstance()->GetVehicleManager()->GetVehicle(vehicleID));
	enemy->SetPosition(SGD::Point(50, 70));

	// Initialize the left Gun
	Gun* gunLeft = new Gun(*Game::GetInstance()->GetGunManager()->GetGun(gunLeftID));
	gunLeft->SetOwner(enemy);
	gunLeft->SetSide("left");

	// Initialize the right Gun
	Gun* gunRight = new Gun(*Game::GetInstance()->GetGunManager()->GetGun(gunRightID));
	gunRight->SetOwner(enemy);
	gunRight->SetSide("right");

	// Set the player's Guns
	enemy->SetGuns(new Guns(gunLeft, gunRight));

	// Add entities to the EntityManager
	m_pEntities->AddEntity(enemy, 0);
	m_pEntities->AddEntity(gunLeft, 0);
	m_pEntities->AddEntity(gunRight, 0);

	// Release those guns + enemy
	enemy->Release();
	gunLeft->Release();
	gunRight->Release();

	return enemy;
}

Entity* GameplayState::CreateTank()
{
	Tank* tank = new Tank(m_hTankBody, m_hTankTurrets, m_hTankMainGun);

	tank->SetPosition(m_pPlayer->GetPosition() - SGD::Vector(0, 800));

	m_pEntities->AddEntity(tank, 0);
	tank->Release();

	return tank;
}

void GameplayState::FixEntityPlacement()
{
	// PLAYER PLACEMENT
	SGD::Point playerStart = m_pPlayer->GetPosition();
	SGD::Point playerPos = playerStart;

	int worldWidth = 8000;
	int worldHeight = 4800;

	// If the player's position has actually changed...
	if ((playerPos.x < 0) || (playerPos.x > worldWidth) || (playerPos.y < 0) || (playerPos.y > worldHeight))
	{
		SGD::Vector cameraPos = Game::GetInstance()->GetCamera();

		// Move the player
		playerPos.x = 1.0f * ((int)(playerPos.x + worldWidth) % worldWidth);
		playerPos.y = 1.0f * ((int)(playerPos.y + worldHeight) % worldHeight);
		m_pPlayer->SetPosition(playerPos);
		dynamic_cast<Vehicle*>(m_pPlayer)->PositionGuns();

		// Move the camera
		cameraPos.x -= playerStart.x - playerPos.x;
		cameraPos.y -= playerStart.y - playerPos.y;
		Game::GetInstance()->SetCamera(cameraPos);
	}

	m_pEntities->RepositionEntities(m_pPlayer->GetPosition(), (float)worldWidth, (float)worldHeight);
}

void GameplayState::AddSpawners()
{
	EnemySpawner* spawner1 = new EnemySpawner(2370, 1515, 0, m_hSpawner);
	m_pEntities->AddEntity(spawner1, 0);
	spawner1->Release();

	EnemySpawner* spawner2 = new EnemySpawner(6688, 2585, 1, m_hSpawner);
	m_pEntities->AddEntity(spawner2, 0);
	spawner2->Release();

	EnemySpawner* spawner3 = new EnemySpawner(800, 3000, 2, m_hSpawner);
	m_pEntities->AddEntity(spawner3, 0);
	spawner3->Release();

	EnemySpawner* spawner4 = new EnemySpawner(4130, 3610, 3, m_hSpawner);
	m_pEntities->AddEntity(spawner4, 0);
	spawner4->Release();
}