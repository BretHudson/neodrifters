/***************************************************************
|	File:		ScoreMessage.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Score Message
***************************************************************/

#include "ScoreMessage.h"

#include "MessageID.h"

ScoreMessage::ScoreMessage(int score) : Message(MessageID::MSG_SCORE)
{
	m_iScore = score;
}

ScoreMessage::~ScoreMessage()
{
}