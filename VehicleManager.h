/***************************************************************
|	File:		VehicleManager.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Vehicle Manager
***************************************************************/

#pragma once

#include <vector>

class Vehicle;

class VehicleManager
{
public:
	VehicleManager();
	~VehicleManager();

	void AddVehicle(Vehicle* vehicle);
	Vehicle* GetVehicle(unsigned int n) const;
	int GetNumVehicles();

private:
	std::vector<Vehicle*> m_vVehicles;
};