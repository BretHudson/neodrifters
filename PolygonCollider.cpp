/***********************************************************************\
|																		|
|	File:			PolygonCollider.cpp									|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		A class that hollds information for a polygon		|
|					collider and does appropriate functions in finding	|
|					projections and such								|
|																		|
\***********************************************************************/

#include "PolygonCollider.h"

#include "../SGD Wrappers/SGD_GraphicsManager.h"

bool Projection::Overlap(Projection p)
{
	if (y < p.x || p.y < x)
		return false; //no overlap
	else
		return true; //yes overlap
	/*if (x < y)
		return (y - p.x) > 0;
	else
		return (x - p.y) > 0;*/
}

Axis::Axis(SGD::Vector v1, SGD::Vector v2)
{
	SGD::Vector axis = v1 - v2;
	this->x = -axis.y;
	this->y = axis.x;
}

PolygonCollider::PolygonCollider()
{

}

PolygonCollider::~PolygonCollider()
{

}

void PolygonCollider::Render(SGD::Point drawPoint)
{
	for (unsigned int i = 0; i < m_vPoints.size(); ++i)
	{
		unsigned int secondindex = i + 1;
		if (secondindex >= m_vPoints.size()) secondindex = 0;

		SGD::Vector v1 = m_vPoints[i] + m_vtOrigin - m_vtOffset;
		SGD::Vector v2 = m_vPoints[secondindex] + m_vtOrigin - m_vtOffset;

		SGD::Point p1 = drawPoint + v1;
		SGD::Point p2 = drawPoint + v2;

		if (secondindex == 0)
			SGD::GraphicsManager::GetInstance()->DrawLine(p1, p2, SGD::Color(255, 0, 0), 1U);
		else
			SGD::GraphicsManager::GetInstance()->DrawLine(p1, p2, SGD::Color(255, 255, 255), 1U);
	}
}

void PolygonCollider::GenerateAxes()
{
	m_vAxes.clear();

	for (unsigned int i = 0; i < m_vPoints.size(); ++i)
	{
		unsigned int secondindex = i + 1;
		if (secondindex >= m_vPoints.size()) secondindex = 0;

		SGD::Vector v1 = m_vPoints[i] + m_vtOrigin - m_vtOffset;
		SGD::Vector v2 = m_vPoints[secondindex] + m_vtOrigin - m_vtOffset;

		Axis edge = Axis(v1, v2);
		m_vAxes.push_back(edge);
	}
}

std::vector<Axis> PolygonCollider::GetAxes()
{
	m_vAxes.clear();

	for (unsigned int i = 0; i < m_vPoints.size(); ++i)
	{
		unsigned int secondindex = i + 1;
		if (secondindex >= m_vPoints.size()) secondindex = 0;

		SGD::Vector v1 = m_vPoints[i] + m_vtOrigin - m_vtOffset;
		SGD::Vector v2 = m_vPoints[secondindex] + m_vtOrigin - m_vtOffset;

		Axis edge = Axis(v1, v2);
		m_vAxes.push_back(edge);
	}
	
	return m_vAxes;
}

void PolygonCollider::SetPoints(std::vector<SGD::Vector> points)
{
	m_vPoints = points;
}

void PolygonCollider::SetPos(SGD::Vector pos)
{
	Collider::SetPos(pos);
	GenerateAxes();
}

void PolygonCollider::SetPos(SGD::Point pos)
{
	Collider::SetPos(pos);
	GenerateAxes();
}

void PolygonCollider::SetOffset(SGD::Vector offset)
{
	Collider::SetOffset(offset);
	GenerateAxes();
}

void PolygonCollider::SetOffset(SGD::Point offset)
{
	Collider::SetOffset(offset);
	GenerateAxes();
}

Projection PolygonCollider::Project(Axis axis)
{
	float min = axis.ComputeDotProduct(m_vPoints[0] + m_vtOrigin - m_vtOffset);
	float max = min;

	for (unsigned int i = 1; i < m_vPoints.size(); ++i)
	{
		float p = axis.ComputeDotProduct(m_vPoints[i] + m_vtOrigin - m_vtOffset);
		if (p < min) min = p;
		if (p > max) max = p;
	}
	
	return Projection(min, max);
}