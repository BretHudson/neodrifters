/***********************************************************************\
|																		|
|	File:			Tank.cpp											|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		The final boss										|
|																		|
\***********************************************************************/

#include "Tank.h"

#include "Image.h"

#include "../SGD Wrappers/SGD_MessageManager.h"

#include "DestroyEntityMessage.h"
#include "ScoreMessage.h"

#include "PolygonCollider.h"

#include "Tags.h"

#include "Game.h"

Tank::Tank(SGD::HTexture body, SGD::HTexture turrets, SGD::HTexture mainGun)
{
	SetHealth(1500);

	m_pBody = new Image(body, 142, 131);
	m_pBody->CenterOrigin();
	
	m_pTurrets = new Image(turrets, 114, 12);
	m_pTurrets->CenterOrigin();

	m_pMainGun = new Image(mainGun, 144, 40);
	m_pMainGun->CenterOrigin();
	m_pMainGun->SetOriginX(20);

	AddGraphic(m_pBody);
	AddGraphic(m_pTurrets);
	AddGraphic(m_pMainGun);

	SetRotation(0.0f);

	m_fMaxspeed = 4.0f;
	m_fSpeed = 4.0f;

	PolygonCollider* poly = new PolygonCollider();

	std::vector<SGD::Vector> points;
	points.push_back(SGD::Vector(-75, -75));
	points.push_back(SGD::Vector(75, -75));
	points.push_back(SGD::Vector(75, 75));
	points.push_back(SGD::Vector(-75, 75));
	poly->SetPoints(points);

	SetCollider(poly);

	AddTag((int)Tags::VEHICLE);
	AddTag((int)Tags::ENEMY);
	AddTag((int)Tags::TANK);
}

Tank::~Tank()
{
	
}

void Tank::Update()
{
	m_fSpeed = 4.0f;

	m_fTurretRotation += 0.02f;
	m_pTurrets->SetRotation(m_fTurretRotation);

	Vehicle::Update();

	if (m_iHealth <= 0)
	{
		SGD::MessageManager::GetInstance()->QueueMessage(new ScoreMessage(500));
		SGD::MessageManager::GetInstance()->QueueMessage(new DestroyEntityMessage(this));
		Game::GetInstance()->Won();
	}
}

void Tank::SetRotation(float angle)
{
	m_pBody->SetRotation(angle + SGD::PI * 0.5f);
	m_pMainGun->SetRotation(angle);

	Entity::SetRotation(angle);
}