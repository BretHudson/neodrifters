/***********************************************************************\
|																		|
|	File:			Tank.h												|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		The final boss										|
|																		|
\***********************************************************************/

#pragma once

#include "Vehicle.h"

class Image;

class Tank :
	public Vehicle
{
public:
	Tank(SGD::HTexture body, SGD::HTexture turrets, SGD::HTexture mainGun);
	~Tank();

	void Update();

	void SetRotation(float ang);

private:
	Image* m_pBody;
	Image* m_pTurrets;
	Image* m_pMainGun;

	float m_fTurretRotation;
};