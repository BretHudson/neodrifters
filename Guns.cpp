/***************************************************************
|	File:		Guns.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A container class that holds a Vehicle's Gun objects
***************************************************************/

#include "Guns.h"
#include "Gun.h"
#include "Vehicle.h"

Guns::Guns(Gun* _left, Gun* _right)
{
	m_pLeft = _left;
	if (m_pLeft)	m_pLeft->AddRef();

	m_pRight = _right;
	if (m_pRight)	m_pRight->AddRef();
}

Guns::~Guns()
{
	if (m_pLeft)	m_pLeft->Release();
	if (m_pRight)	m_pRight->Release();
}

void Guns::PositionGuns(SGD::Point ptLeft, SGD::Point ptRight, float angle)
{
	if (m_pLeft)
	{
		m_pLeft->SetPosition(ptLeft);
		m_pLeft->SetRotation(angle);
	}

	if (m_pRight)
	{
		m_pRight->SetPosition(ptRight);
		m_pRight->SetRotation(angle);
	}
}

void Guns::Shoot(bool pressed, bool down)
{
	bool lshot, rshot;

	if (m_pLeft)
		lshot = m_pLeft->Shoot(pressed, down);

	if (m_pRight)
		rshot = m_pRight->Shoot(pressed, down);

	if ((lshot) || (rshot))
		m_pOwner->Screenshake(4.0f);
}