/***************************************************************
|	File:		Gun.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	An entity that shoots projectiles (Bullet)
***************************************************************/

#include "Gun.h"
#include "Graphic.h"

#include "../SGD Wrappers/SGD_MessageManager.h"

#include "CreateBulletMessage.h"

Gun::Gun()
{
}

Gun::Gun(const Gun& other) : Entity(other)
{
	m_sName = other.m_sName;
	m_iID = other.m_iID;

	m_iReloadTimer = other.m_iReloadTimer;
	m_iReloadTimeout = other.m_iReloadTimeout;

	m_bAlternate = other.m_bAlternate;
	m_bAutomatic = other.m_bAutomatic;
}

Gun::~Gun()
{
	
}

void Gun::Update()
{
	--m_iReloadTimer;
	m_vGraphics[0]->SetRotation(GetRotation());
	if (m_sSide == "right")
		m_vGraphics[0]->SetScaleY(-1);
}

void Gun::Render()
{
	m_vGraphics[0]->SetRotation(GetRotation());
	Entity::Render();
}

bool Gun::Shoot(bool pressed, bool down)
{
	if (((m_bAutomatic) && (down)) || (pressed))
	{
		if (m_iReloadTimer < 0.0f)
		{
			m_iReloadTimer = m_iReloadTimeout;
		
 			SGD::MessageManager::GetInstance()->QueueMessage(new CreateBulletMessage(GetPosition(), m_iID, m_pOwner));

			return true;
		}
	}

	return false;
}