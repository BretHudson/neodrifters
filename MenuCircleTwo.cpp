/***************************************************************
|	File:		MenuCircleTwo.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	MenuCircle Two
***************************************************************/

#include "MenuCircleTwo.h"


MenuCircleTwo::MenuCircleTwo(SGD::HTexture bright, SGD::HTexture dim) : MenuCircle(bright, dim)
{
}


MenuCircleTwo::~MenuCircleTwo()
{
}

void MenuCircleTwo::Update()
{
	MenuCircle::Update();

	//
}

void MenuCircleTwo::Render()
{
	MenuCircle::Render();

	//
}