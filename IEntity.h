/***************************************************************
|	File:		IEntity.h
|	Author:		
|	Course:		
|	Purpose:	IEntity class is the interface for all game entities
***************************************************************/

#ifndef IENTITY_H
#define IENTITY_H


#include "../SGD Wrappers/SGD_Geometry.h"	// Rectangle type

class Collider;
class Entity;


/**************************************************************/
// IEntity class
//	- interface base class:
//		- virtual methods for children classes to override
//		- no data members
class IEntity
{
	/**********************************************************/
	// Destructor MUST be virtual
public:
	IEntity( void )				= default;
protected:
	virtual ~IEntity( void )	= default;

public:
	/**********************************************************/
	// Interface:
	//	- pure virtual methods MUST be overridden
	virtual void	Update		( void )	= 0;
	virtual void	Render		( void )				= 0;

	virtual SGD::Rectangle GetRect( void )	const		= 0;
	virtual int		GetType		( void )	const		= 0;
	virtual void	HandleCollision(const IEntity* pOther) = 0;

	int					GetLayer(void) const			{ return m_iLayer; }

	void				SetLayer(int			layer)	{ m_iLayer = layer; }

	Collider* GetCollider(void) const { return m_pCollider; }

	virtual Collider* Collide(float x, float y, int tags[]) = 0;
	virtual Collider* Collide(float x, float y, Collider* c) = 0;
	virtual Collider* Collide(float x, float y, Entity* e) = 0;
	
	/**********************************************************/
	// Reference Counting:
	//	- track how many 'owners' are pointing to this entity
	virtual void	AddRef		( void )				= 0;
	virtual void	Release(void) = 0;

protected:
	int m_iLayer = 0;

	Collider* m_pCollider = nullptr;
};

#endif //IENTITY_H
