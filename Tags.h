/***************************************************************
|	File:		Tags.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Tags enum declares the tag enumerators
***************************************************************/

#ifndef TAGS_H
#define TAGS_H

enum class Tags
{
	NONE,
	VEHICLE,
	PLAYER,
	ENEMY,
	TANK,
	BULLET,
	SOLID,
	PORTAL
};


#endif