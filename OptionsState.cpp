/***************************************************************
|	File:		OptionsState.cpp
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Options State
***************************************************************/

#include "OptionsState.h"

#include "Image.h"

#include "Input.h"

#include "Game.h"
#include "MainMenuState.h"

OptionsState* OptionsState::GetInstance()
{
	static OptionsState s_Instance;
	return &s_Instance;
}

void OptionsState::Enter()
{
	m_hBackground = SGD::GraphicsManager::GetInstance()->LoadTexture("resource/graphics/optionsmenu/be_optionsmenu.png");
	m_pBackground = new Image(m_hBackground, 960, 544);
}

void OptionsState::Exit()
{
	SGD::GraphicsManager::GetInstance()->UnloadTexture(m_hBackground);
	delete m_pBackground;
}

bool OptionsState::Input()
{
	// Press Escape to return to Main Menu
	if (Input::CheckPressed("menuback"))
	{
		Game::GetInstance()->ChangeState(MainMenuState::GetInstance());

		return true;
	}

	return true;
}

void OptionsState::Update()
{
	//
}

void OptionsState::Render()
{
	m_pBackground->Render(SGD::Point(0, 0));
}