/***********************************************************************\
|																		|
|	File:			Anim.h												|
|	Author:			Bret Hudson										|
|	Course:			SGD1404												|
|	Purpose:		Holds the data for an individual animation			|
|																		|
\***********************************************************************/

#pragma once

#include <vector>

class Anim
{
public:
	Anim();
	~Anim();

	void Update();

	void AddFrame(int frame);
	void SetFramerate(int framerate) { m_iFramerate = framerate; }
	int GetFrame() const;

private:
	std::vector<int> m_vFrames;
	int m_iFramerate = 1;
	int m_iTimer = 0;
	int m_iCurFrameIndex = 0;
};

