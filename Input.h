/***************************************************************
|	File:		Input.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Handles Input and maps keys to strings
***************************************************************/

#pragma once

#include <cstring>
#include <map>
#include <vector>
#include <initializer_list>

#include "../SGD Wrappers/SGD_InputManager.h"

//class Input;

namespace Types
{
	enum Axes { LEFTX, LEFTY, RIGHTX, RIGHTY };
	enum States { NONE = -1, PRESSED, DOWN, RELEASED };
};

typedef Types::Axes AxisTypes;
typedef Types::States AxisStates;

struct InputAxis
{
	AxisTypes type;
	int dir;
	AxisStates state = AxisStates::NONE;
};

class Input
{
private:
	Input();
	~Input();

private:
	// Key containers typedefs
	typedef std::vector<SGD::Key> KeyVector;
	typedef std::vector<SGD::Key>::iterator KeyVectorIter;
	typedef std::pair<std::string, std::vector<SGD::Key>> KeyPair;
	typedef std::map<std::string, std::vector<SGD::Key>> KeyMap;
	typedef std::map<std::string, std::vector<SGD::Key>>::iterator KeyMapIter;

	// Button containers typedef
	typedef std::vector<int> ButtonVector;
	typedef std::vector<int>::iterator ButtonVectorIter;
	typedef std::pair<std::string, std::vector<int>> ButtonPair;
	typedef std::map<std::string, std::vector<int>> ButtonMap;
	typedef std::map<std::string, std::vector<int>>::iterator ButtonMapIter;

	// Axis containers typedef
	typedef std::pair<std::string, InputAxis> AxisPair;
	typedef std::map<std::string, InputAxis> AxisMap;
	typedef std::map<std::string, InputAxis>::iterator AxisMapIter;

public:
	static void Update();

	static void SetDeadzone(float zone) { deadzone = zone; }

	static void RegisterKey(std::string name, SGD::Key key);
	static void RegisterKeys(std::string name, std::initializer_list<SGD::Key> keys);
	static void RegisterButton(std::string name, int button);
	static void RegisterButtons(std::string name, std::initializer_list<int> buttons);
	static void RegisterAxis(std::string negName, std::string posName, AxisTypes axis);

	static float GetAxisValue(AxisTypes type);
	static float GetAxisValue(std::string name);

	static bool CheckPressed(std::string name);
	static bool CheckDown(std::string name);
	static bool CheckReleased(std::string name);

	static int GetMouseX();
	static int GetMouseY();
	static SGD::Point GetMouseXY();

	static bool GetMousePressed(SGD::Key key);
	static bool GetMouseDown(SGD::Key key);
	static bool GetMouseReleased(SGD::Key key);

private:
	static KeyMap keys;
	static ButtonMap buttons;
	static AxisMap axes;

	static float deadzone;
};

