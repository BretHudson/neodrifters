/***************************************************************
|	File:		PortalDestroyedMessage.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	Portal Destroyed Message
***************************************************************/

#pragma once

#include "..\SGD Wrappers\SGD_Message.h"

class Entity;

class PortalDestroyedMessage :
	public SGD::Message
{
public:
	PortalDestroyedMessage(Entity* entity);
	~PortalDestroyedMessage();

	Entity* GetEntity(void) const { return m_pEntity; }

private:
	Entity* m_pEntity;
};

