/***************************************************************
|	File:		Bullet.h
|	Author:		Bret Hudson
|	Course:		SGD1404
|	Purpose:	A class that acts as a projectile
***************************************************************/

#pragma once

#include "Entity.h"

class Vehicle;
class Text;

class Bullet :
	public Entity
{
public:
	Bullet();
	Bullet(const Bullet& other);
	~Bullet();

	int	GetType(void) const override { return ((m_bHoming) ? ENT_MISSILE : ENT_BULLET); }

	void Update();
	void Render();

	void Reposition(float width, float height);

	// Accessors
	float GetSpeed(void) const { return m_fSpeed; }
	bool GetExplosive(void) const { return m_bExplosive; }
	int GetLife(void) const { return m_iLife; }
	bool GetHoming(void) const { return m_bHoming; }
	Vehicle* GetTarget(void) const { return m_pTarget; }
	float GetDamage(void) const { return m_fDamage; }
	Entity* GetOwner(void) const { return m_pOwner; }

	// Mutators
	void SetSpeed(float speed) { m_fSpeed = speed; }
	void SetExplosive(bool explosive) { m_bExplosive = explosive; }
	void SetLife(int life) { m_iLife = life; }
	void SetRotation(float angle);
	void SetHoming(bool homing) { m_bHoming = homing; }
	void SetTarget(Vehicle* target);
	void SetDamage(float damage) { m_fDamage = damage; }
	void SetOwner(Entity* owner);

private:
	float m_fDamage = 0;
	bool m_bExplosive = false;

	float m_fSpeed = 0;

	int m_iLife = 0;

	bool m_bHoming = false;
	Vehicle* m_pTarget;
	Entity* m_pOwner = nullptr;

	SGD::HAudio m_hHit = SGD::INVALID_HANDLE;

	Text* volumeText;
};

